# NOTE K-Athena is not maintained any more. Please transition to the successor AthenaPK (with AMR support), see https://github.com/parthenon-hpc-lab/athenapk

K-Athena
======

K-Athena is a performance portable structured grid finite volume magnetohydrodynamics code.

The name K-Athena originates from the fundamental components of the code:
- the performance portability library [Kokkos](https://github.com/kokkos/kokkos), and
- the CPU radiation GRMHD code [Athena++](https://github.com/PrincetonUniversity/athena-public-version).

Performance portability broadly refers to achieving consistent levels of performance across heterogeneous platforms using as little architecture-dependent code as possible.

The Kokkos basis of K-Athena allows simulation to be run efficiently on many different architectures such as CPU and Intel Xeon Phis (with a high level of vectorization) and Nvidia GPUs.
K-Athena has been tested and demonstrated good scalability up to several thousands of nodes on NASA's Electra (Intel Skylake architecture), ALCF's Theta (Intel Knights Landing architecture), and OLCF's Summit (IBM Power 9 CPUs and Nvidia Volta V100 GPUs).

For more details see the [code paper](https://arxiv.org/abs/1905.04341).

## Building and running simulations

Given that Kokkos is only used to achieve performance portability, the core algorithms and methods are unchanged.
Therefore, the Athena++ [documentation](https://github.com/PrincetonUniversity/athena-public-version/wiki) generally applies to K-Athena, too.
Moreover, the Athena++ output/data dump format has not been altered. Thus, simulations originally run with Athena++ can be continued/restarted using K-Athena.

Specific instructions on how to compile K-Athena can be found in the K-Athena [wiki](https://gitlab.com/pgrete/kathena/wikis/building-and-running-k-athena).

Please note that not all features of Athena++ have been ported yet.
Currently supported are non-relativistic hydro and MHD simulations using Cartesian coordinates. If you are interested in porting additional features, see [Contributing](#contributing)

### Running turbulence simulations

To run driven turbulence simulations in a performance portable way a new problem
generator has been introduced to K-Athena, which is not part of Athena++.

More details can be found in the
[K-Athena wiki](https://gitlab.com/pgrete/kathena/wikis/turbulence).

## Support/Help/Questions

Please file an issue for anything related to
- how to build K-Athena or build problems,
- how to run on K-Athena,
- bugs,
- feature requests, and
- questions in general.

Note that the resources of the current development team are limited and support will be provided on a best effort basis.

## Contributing
Contributions to K-Athena in any form are highly encouraged.
These include updates to the documentation, fixing bugs, porting additional features, or adding new functionality.
Please open an issue first for a general discussion of the contribution and support/guidance in how to implement/realize it, and eventually open a merge request from your branch/fork.

## Acknowledgement
The development of K-Athena has been enabled by various factors.
Both Athena++ and Kokkos are open source codes and their community's commitment to open science made K-Athena possible in first place.
Moreover, the K-Athena development was supported by the NASA Astrophysics Theory Program grant #NNX15AP39G and by several computing grants including allocations on NASA Pleiades (SMD-16-7720), OLCF Titan (AST133), OLCF Summit (AST146), ALCF Theta (athena performance), XSEDE Comet (TG-AST090040), and Michgian State University’s High Performance Computing Center.

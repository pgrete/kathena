//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file flux_correction_fc.cpp
//  \brief functions that perform flux correction for FACE_CENTERED variables

// C++ headers
#include <iostream>   // endl
#include <iomanip>
#include <sstream>    // stringstream
#include <stdexcept>  // runtime_error
#include <string>     // c_str()
#include <cstring>    // memcpy
#include <cstdlib>
#include <cmath>

// Athena++ classes headers
#include "bvals.hpp"
#include "../athena.hpp"
#include "../globals.hpp"
#include "../athena_arrays.hpp"
#include "../mesh/mesh.hpp"
#include "../hydro/hydro.hpp"
#include "../eos/eos.hpp"
#include "../field/field.hpp"
#include "../coordinates/coordinates.hpp"
#include "../parameter_input.hpp"
#include "../utils/buffer_utils.hpp"

// MPI header
#ifdef MPI_PARALLEL
#include <mpi.h>
#endif

//----------------------------------------------------------------------------------------
//! \fn int BoundaryValues::LoadEMFBoundaryBufferSameLevel(Kokkos::View<Real*,
//                                                         DevSpace>& buf,
//                                                         const NeighborBlock& nb)
//  \brief Set EMF correction buffers for sending to a block on the same level
int BoundaryValues::LoadEMFBoundaryBufferSameLevel(Kokkos::View<Real*,DevSpace>& buf,
                                                   const NeighborBlock& nb) {
  MeshBlock *pmb=pmy_block_;
  auto e1=pmb->pfield->e.x1e.get_KView3D();
  auto e2=pmb->pfield->e.x2e.get_KView3D();
  auto e3=pmb->pfield->e.x3e.get_KView3D();

  Real qomL = qshear_*Omega_0_*x1size_;
  auto bx1 =pmb->pfield->b.x1f.get_KView3D();

  //Number of threads in each dimension
  int ni,nj,nk;

  //Loop boundaries 
  int is,js,ks;
  int ie,je,ke;

  int p=0;
  if (nb.type==NEIGHBOR_FACE) {
    if (pmb->block_size.nx3 > 1) { // 3D
      // x1 direction
      if (nb.fid==INNER_X1 || nb.fid==OUTER_X1) {
        int i;
        if (nb.fid==INNER_X1) i=pmb->is;
        else i=pmb->ie+1;
        // pack e2
        ks = pmb->ks; ke = pmb->ke+1; //Determine loop bounds
        js = pmb->js; je = pmb->je;
        nk = ke+1-ks; //Determine number of threads needed
        nj = je+1-js;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x1 pack e2",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({ks,js},{ke+1,je+1}),
          KOKKOS_LAMBDA (int k, int j) {
            buf[p+ (j-js + nj*(k-ks )) ]=e2(k,j,i); //Fill with e2
          });
        p += nj*nk; //offset p

        // pack e3
        // shift azmuthal velocity if shearing boundary blocks
        if (nb.shear && nb.fid==INNER_X1) {
          for (int k=pmb->ks; k<=pmb->ke; k++) {
            for (int j=pmb->js; j<=pmb->je+1; j++)
              buf[p++]=e3(k,j,i)-0.5*qomL*(bx1(k,j,i)+bx1(k,j-1,i));
          }
        } else if (nb.shear && nb.fid==OUTER_X1) {
          for (int k=pmb->ks; k<=pmb->ke; k++) {
            for (int j=pmb->js; j<=pmb->je+1; j++)
              buf[p++]=e3(k,j,i)+0.5*qomL*(bx1(k,j,i)+bx1(k,j-1,i));
          }
        } else {
          ks = pmb->ks; ke = pmb->ke;
          js = pmb->js; je = pmb->je+1;
          nk = ke+1-ks;
          nj = je+1-js;
          Kokkos::parallel_for(
            "LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x1 pack e3",
            Kokkos::MDRangePolicy<Kokkos::Rank<2>>({ks,js},{ke+1,je+1}),
            KOKKOS_LAMBDA (int k, int j) {
              buf[p+ (j-js + nj*(k-ks )) ]=e3(k,j,i);
            });
          p += nj*nk; //offset p
        }
        // x2 direction
      } else if (nb.fid==INNER_X2 || nb.fid==OUTER_X2) {
        int j;
        if (nb.fid==INNER_X2) j=pmb->js;
        else j=pmb->je+1;
        // pack e1
        ks = pmb->ks; ke = pmb->ke+1;
        is = pmb->is; ie = pmb->ie;
        nk = ke+1-ks;
        ni = ie+1-is;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x2 pack e1",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({ks,is},{ke+1,ie+1}),
          KOKKOS_LAMBDA (int k, int i) {
            buf[p+ (i-is + ni*(k-ks )) ]=e1(k,j,i);
          });
        p += ni*nk; //offset p
        // pack e3
        ks = pmb->ks; ke = pmb->ke;
        is = pmb->is; ie = pmb->ie+1;
        nk = ke+1-ks;
        ni = ie+1-is;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x2 pack e3",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({ks,is},{ke+1,ie+1}),
          KOKKOS_LAMBDA (int k, int i) {
            buf[p+ (i-is + ni*(k-ks )) ]=e3(k,j,i);
          });
        p += ni*nk; //offset p
      // x3 direction
      } else if (nb.fid==INNER_X3 || nb.fid==OUTER_X3) {
        int k;
        if (nb.fid==INNER_X3) k=pmb->ks;
        else k=pmb->ke+1;
        // pack e1
        js = pmb->js; je = pmb->je+1;
        is = pmb->is; ie = pmb->ie;
        nj = je+1-js;
        ni = ie+1-is;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x3 pack e1",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({js,is},{je+1,ie+1}),
          KOKKOS_LAMBDA (int j, int i) {
            buf[p+ (i-is + ni*(j-js )) ]=e1(k,j,i);
          });
        p += ni*nj; //offset p
        // pack e2
        js = pmb->js; je = pmb->je;
        is = pmb->is; ie = pmb->ie+1;
        nj = je+1-js;
        ni = ie+1-is;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x3 pack e2",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({js,is},{je+1,ie+1}),
          KOKKOS_LAMBDA (int j, int i) {
            buf[p+ (i-is + ni*(j-js )) ]=e2(k,j,i);
          });
        p += ni*nj; //offset p
      }
    } else if (pmb->block_size.nx2 > 1) { // 2D
      int k=pmb->ks;
      // x1 direction
      if (nb.fid==INNER_X1 || nb.fid==OUTER_X1) {
        int i;
        if (nb.fid==INNER_X1) i=pmb->is;
        else i=pmb->ie+1;
        // pack e2
        // shift azimuthal velocity for x-z shearing
        if (SHEARING_BOX) {
          if (ShBoxCoord_==2 && (pmb->loc.lx1==0) && (nb.ox1==-1)) {
            for (int j=pmb->js; j<=pmb->je; j++)
              buf[p++]=e2(k,j,i)+qomL*bx1(k,j,i);
          } else if (ShBoxCoord_==2 && (pmb->loc.lx1==(pmb->pmy_mesh->nrbx1-1))
                                   && nb.ox1==1) {
            for (int j=pmb->js; j<=pmb->je; j++)
              buf[p++]=e2(k,j,i)-qomL*bx1(k,j,i);
          } else {
            for (int j=pmb->js; j<=pmb->je; j++)
              buf[p++]=e2(k,j,i);
          }
        } else {
          js = pmb->js; je = pmb->je;
          nj = je+1-js;
          Kokkos::parallel_for(
            "LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x1 pack e2",
            Kokkos::RangePolicy<>(js,je+1),
            KOKKOS_LAMBDA (int j){
              buf[p + (j -js)] = e2(k,j,i);
            });
          p += nj;
        }
        // pack e3
        js = pmb->js; je = pmb->je+1;
        nj = je+1-js;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x1 pack e3",
          Kokkos::RangePolicy<>(js,je+1),
          KOKKOS_LAMBDA (int j){
            buf[p + (j -js)] = e3(k,j,i);
          });
        p += nj;
      // x2 direction
      } else if (nb.fid==INNER_X2 || nb.fid==OUTER_X2) {
        int j;
        if (nb.fid==INNER_X2) j=pmb->js;
        else j=pmb->je+1;
        // pack e3
        is = pmb->is; ie = pmb->ie;
        ni = ie+1-is;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x2 pack e1",
          Kokkos::RangePolicy<>(is,ie+1),
          KOKKOS_LAMBDA (int i){
            buf[p + (i -is)] = e1(k,j,i);
          });
        p += ni;
        // pack e3
        is = pmb->is; ie = pmb->ie+1;
        ni = ie+1-is;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x2 pack e3",
          Kokkos::RangePolicy<>(is,ie+1),
          KOKKOS_LAMBDA (int i){
            buf[p + (i -is)] = e3(k,j,i);
          });
        p += ni;
      }
    } else { // 1D
      int i, j=pmb->js, k=pmb->ks;
      if (nb.fid==INNER_X1) i=pmb->is;
      else i=pmb->ie+1;
      // pack e2 and e3
      Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_FACE 1D",
        Kokkos::RangePolicy<>(0,1),
        KOKKOS_LAMBDA (int x){
          buf[p + 0] = e2(k,j,i);
          buf[p + 1] = e3(k,j,i);
        });
      p += 2;
    }
  } else if (nb.type==NEIGHBOR_EDGE) {
    // x1x2 edge (both 2D and 3D)
    if (nb.eid>=0 && nb.eid<4) {
      int i, j;
      if ((nb.eid&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((nb.eid&2)==0) j=pmb->js;
      else j=pmb->je+1;
      // shift azmuthal velocity if shearing boundary blocks
      if (nb.shear and nb.ox1==-1) {
        for (int k=pmb->ks; k<=pmb->ke; k++)
          buf[p++]=e3(k,j,i)-0.5*qomL*(bx1(k,j,i)+bx1(k,j-1,i));
      } else if (nb.shear and nb.ox1==1) {
        for (int k=pmb->ks; k<=pmb->ke; k++)
          buf[p++]=e3(k,j,i)+0.5*qomL*(bx1(k,j,i)+bx1(k,j-1,i));
      } else {
      // pack e3
        ks = pmb->ks; ke = pmb->ke;
        nk = ke+1-ks;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_EDGE x1x2 pack e3",
          Kokkos::RangePolicy<>(ks,ke+1),
          KOKKOS_LAMBDA (int k){
            buf[p + (k -ks)] = e3(k,j,i);
          });
        p += nk;
      }
      // x1x3 edge
    } else if (nb.eid>=4 && nb.eid<8) {
      int i, k;
      if ((nb.eid&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((nb.eid&2)==0) k=pmb->ks;
      else k=pmb->ke+1;
      // pack e2
      // shift azimuthal velocity for x-z shearing
      if (SHEARING_BOX) {
        if (ShBoxCoord_==2 && (pmb->loc.lx1==0) && (nb.ox1==-1))   {
          for (int j=pmb->js; j<=pmb->je; j++)
            buf[p++]=e2(k,j,i)+qomL*bx1(k,j,i);
        } else if (ShBoxCoord_==2 && (pmb->loc.lx1==(pmb->pmy_mesh->nrbx1-1)) &&
                   nb.ox1==1) {
          for (int j=pmb->js; j<=pmb->je; j++)
            buf[p++]=e2(k,j,i)-qomL*bx1(k,j,i);
        } else {
          for (int j=pmb->js; j<=pmb->je; j++)
            buf[p++]=e2(k,j,i);
        }
      } else {
        js = pmb->js; je = pmb->je;
        nj = je+1-js;
        Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_EDGE x1x3 pack e2",
          Kokkos::RangePolicy<>(js,je+1),
          KOKKOS_LAMBDA (int j){
            buf[p + (j -js)] = e2(k,j,i);
          });
        p += nj;
      }
      // x2x3 edge
    } else if (nb.eid>=8 && nb.eid<12) {
      int j, k;
      if ((nb.eid&1)==0) j=pmb->js;
      else j=pmb->je+1;
      if ((nb.eid&2)==0) k=pmb->ks;
      else k=pmb->ke+1;
      // pack e1
      is = pmb->is; ie = pmb->ie;
      ni = ie+1-is;
      Kokkos::parallel_for("LoadEMFBoundaryBufferSameLevel NEIGHBOR_EDGE x2x3 pack e1",
        Kokkos::RangePolicy<>(is,ie+1),
        KOKKOS_LAMBDA (int i){
          buf[p + (i -is)] = e1(k,j,i);
        });
      p += ni;
    }
  }
  return p;
}


//----------------------------------------------------------------------------------------
//! \fn int BoundaryValues::LoadEMFBoundaryBufferToCoarser(Kokkos::View<Real*,
//                                                         DevSpace>& buf,
//                                                         const NeighborBlock& nb)
//  \brief Set EMF correction buffers for sending to a block on the coarser level

int BoundaryValues::LoadEMFBoundaryBufferToCoarser(Kokkos::View<Real*,DevSpace>& buf,
                                                   const NeighborBlock& nb) {
  MeshBlock *pmb=pmy_block_;
  Coordinates *pco=pmb->pcoord;
  auto e1 =pmb->pfield->e.x1e.get_KView3D();
  auto e2 =pmb->pfield->e.x2e.get_KView3D();
  auto e3 =pmb->pfield->e.x3e.get_KView3D();

  // use the surface area aray as the edge length array
  auto le1 =sarea_[0].get_KView1D();
  auto le2 =sarea_[1].get_KView1D();

  int p=0;

  //Just ignore polar, gr, etc. and throw an error if it's encountered
  if (COORDINATE_SYSTEM != "cartesian" || GENERAL_RELATIVITY ) {
    throw std::runtime_error(
      "BoundaryValues::LoadEMFBoundaryBufferToCoarser doesn't support non-cartesian grids"
      " nor general relativity");
  }

  //Grab the useful arrays for cartesian
  auto dx1f = pco->dx1f.get_KView1D();
  auto dx2f = pco->dx2f.get_KView1D();
  auto dx3f = pco->dx3f.get_KView1D();

  //Number of threads
  int ni, nj, nk;

  //Loop bounds
  int is, js, ks;
  int ie, je, ke;

  if (nb.type==NEIGHBOR_FACE) {
    if (pmb->block_size.nx3 > 1) { // 3D
      // x1 direction
      if (nb.fid==INNER_X1 || nb.fid==OUTER_X1) {
        int i;
        if (nb.fid==INNER_X1) i=pmb->is;
        else i=pmb->ie+1;
        // restrict and pack e2
        ks = pmb->ks; ke = pmb->ke+1;
        js = pmb->js; je = pmb->je;
        nk = (ke - ks)/2+1;
        nj = (je - js)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 3D x1 pack e2",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{nk,nj}),
          KOKKOS_LAMBDA (int idx_k, int idx_j) {
            int k = idx_k*2+ks;
            int j = idx_j*2+js;
            Real el1=dx2f(j);
            Real el2=dx2f(j+1);
            buf[p+idx_j + nj*idx_k]=(e2(k,j,i)*el1+e2(k,j+1,i)*el2)/(el1+el2);

          });
        p += nj*nk; //offset p
        // restrict and pack e3
        ks = pmb->ks; ke = pmb->ke;
        js = pmb->js; je = pmb->je+1;
        nk = (ke - ks)/2+1;
        nj = (je - js)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 3D x1 pack e3",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{nk,nj}),
          KOKKOS_LAMBDA (int idx_k, int idx_j) {
            int k = idx_k*2+ks;
            int j = idx_j*2+js;
            Real el1=dx3f(k);
            Real el2=dx3f(k+1);
            buf[p+idx_j + nj*idx_k]=(e3(k,j,i)*el1+e3(k+1,j,i)*el2)/(el1+el2);
          });
        p += nj*nk; //offset p
        // x2 direction
      } else if (nb.fid==INNER_X2 || nb.fid==OUTER_X2) {
        int j;
        if (nb.fid==INNER_X2) j=pmb->js;
        else j=pmb->je+1;
        bool pole = pco->IsPole(j);
        // restrict and pack e1
        ks = pmb->ks; ke = pmb->ke+1;
        is = pmb->is; ie = pmb->ie;//There was a typo here in the original source
        nk = (ke - ks)/2+1;
        ni = (ie - is)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 3D x2 pack e1",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{nk,ni}),
          KOKKOS_LAMBDA (int idx_k, int idx_i) {
            int k = idx_k*2+ks;
            int i = idx_i*2+is;
            Real le1_i =dx1f(i);
            Real le1_i1=dx1f(i+1);
            buf[p+idx_i + ni*idx_k]=(e1(k,j,i)*le1_i+e1(k,j,i+1)*le1_i1)/(le1_i+le1_i1);
          });
        p += ni*nk; //offset p

        // restrict and pack e3
        ks = pmb->ks; ke = pmb->ke;
        is = pmb->is; ie = pmb->ie+1;
        nk = (ke - ks)/2+1;
        ni = (ie - is)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 3D x2 pack e3",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{nk,ni}),
          KOKKOS_LAMBDA (int idx_k, int idx_i) {
            int k = idx_k*2+ks;
            int i = idx_i*2+is;
            Real le1 = dx3f(k);
            Real le2 = dx3f(k+1);
            buf[p+idx_i + ni*idx_k]=(e3(k,j,i)*le1+e3(k+1,j,i)*le2)/(le1+le2);
          });
        p += ni*nk; //offset p

      // x3 direction
      } else if (nb.fid==INNER_X3 || nb.fid==OUTER_X3) {
        int k;
        if (nb.fid==INNER_X3) k=pmb->ks;
        else k=pmb->ke+1;
        // restrict and pack e1
        js = pmb->js; je = pmb->je+1;
        is = pmb->is; ie = pmb->ie;
        nj = (je - js)/2+1;
        ni = (ie - is)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 3D x3 pack e1",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{nj,ni}),
          KOKKOS_LAMBDA (int idx_j, int idx_i) {
            int j = idx_j*2+js;
            int i = idx_i*2+is;
            Real le1_i  = dx1f(i);
            Real le1_i1 = dx1f(i+1);
            buf[p+idx_i + ni*idx_j]=(e1(k,j,i)*le1_i+e1(k,j,i+1)*le1_i1)/(le1_i+le1_i1);
          });
        p += ni*nj; //offset p

        // restrict and pack e2
        js = pmb->js; je = pmb->je;
        is = pmb->is; ie = pmb->ie+1;
        nj = (je - js)/2+1;
        ni = (ie - is)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 3D x3 pack e2",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{nj,ni}),
          KOKKOS_LAMBDA (int idx_j, int idx_i) {
            int j = idx_j*2+js;
            int i = idx_i*2+is;
            Real le1 = dx2f(j);
            Real le2 = dx2f(j+1);
            buf[p+idx_i + ni*idx_j]=(e2(k,j,i)*le1+e2(k,j+1,i)*le2)/(le1+le2);
          });
        p += ni*nj; //offset p
      }
    } else if (pmb->block_size.nx2 > 1) { // 2D
      int k=pmb->ks;
      // x1 direction
      if (nb.fid==INNER_X1 || nb.fid==OUTER_X1) {
        int i;
        if (nb.fid==INNER_X1) i=pmb->is;
        else i=pmb->ie+1;
        // restrict and pack e2
        js = pmb->js; je = pmb->je;
        nj = (je - js)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 2D x1 pack e2",
          Kokkos::RangePolicy<>(0,nj),
          KOKKOS_LAMBDA (int idx_j){
            int j = idx_j*2+js;
            Real el1=dx2f(j);
            Real el2=dx2f(j+1);
            buf[p + idx_j] = (e2(k,j,i)*el1+e2(k,j+1,i)*el2)/(el1+el2);
          });
        p += nj;
       
        // pack e3
        js = pmb->js; je = pmb->je+1;
        nj = (je - js)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 2D x1 pack e3",
          Kokkos::RangePolicy<>(0,nj),
          KOKKOS_LAMBDA (int idx_j){
            int j = idx_j*2+js;
            buf[p + idx_j] = e3(k,j,i);
          });
        p += nj;

        // x2 direction
      } else if (nb.fid==INNER_X2 || nb.fid==OUTER_X2) {
        int j;
        if (nb.fid==INNER_X2) j=pmb->js;
        else j=pmb->je+1;
        bool pole = pco->IsPole(j);
        // restrict and pack e1
        is = pmb->is; ie = pmb->ie;
        ni = (ie - is)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 2D x2 pack e1",
          Kokkos::RangePolicy<>(0,ni),
          KOKKOS_LAMBDA (int idx_i){
            int i = idx_i*2+is;
            Real le1_i  = dx1f(i);
            Real le1_i1 = dx1f(i+1);
            buf[p + idx_i] = (e1(k,j,i)*le1_i+e1(k,j,i+1)*le1_i1)/(le1_i+le1_i1);
          });
        p += ni;
        

        // pack e3
        is = pmb->is; ie = pmb->ie+1;
        ni = (ie - is)/2+1;
        Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 2D x2 pack e3",
          Kokkos::RangePolicy<>(0,ni),
          KOKKOS_LAMBDA (int idx_i){
            int i = idx_i*2+is;
            buf[p + idx_i] = e3(k,i,i);
          });
        p += ni;
      }
    } else { // 1D
      int i, j=pmb->js, k=pmb->ks;
      if (nb.fid==INNER_X1) i=pmb->is;
      else i=pmb->ie+1;
      // pack e2 and e3
      Kokkos::parallel_for("LoadEMFBoundaryBufferToCoarser NEIGHBOR_FACE 1D",
        Kokkos::RangePolicy<>(0,1),
        KOKKOS_LAMBDA (int x){
          buf[p + 0] = e2(k,j,i);
          buf[p + 1] = e3(k,j,i);
        });
      p += 2;
    }
  } else if (nb.type==NEIGHBOR_EDGE) {
    if (pmb->block_size.nx3 > 1) { // 3D
      // x1x2 edge
      if (nb.eid>=0 && nb.eid<4) {
        int i, j;
        if ((nb.eid&1)==0) i=pmb->is;
        else i=pmb->ie+1;
        if ((nb.eid&2)==0) j=pmb->js;
        else j=pmb->je+1;
        bool pole = pco->IsPole(j);
        // restrict and pack e3
        ks = pmb->ks; ke = pmb->ke;
        nk = (ke - ks)/2+1;
        Kokkos::parallel_for(
          "LoadEMFBoundaryBufferToCoarser NEIGHBOR_EDGE 3D x1x2 pack e3",
          Kokkos::RangePolicy<>(0,nk),
          KOKKOS_LAMBDA (int idx_k){
            int k = idx_k*2+ks;
            Real el1 = dx3f(k);
            Real el2 = dx3f(k+1);
            buf[p + idx_k] = (e3(k,j,i)*el1+e3(k+1,j,i)*el2)/(el1+el2);
          });
        p += nk;

        // x1x3 edge
      } else if (nb.eid>=4 && nb.eid<8) {
        int i, k;
        if ((nb.eid&1)==0) i=pmb->is;
        else i=pmb->ie+1;
        if ((nb.eid&2)==0) k=pmb->ks;
        else k=pmb->ke+1;
        // restrict and pack e2
        js = pmb->js; je = pmb->je;
        nj = (je - js)/2+1;
        Kokkos::parallel_for(
          "LoadEMFBoundaryBufferToCoarser NEIGHBOR_EDGE 3D x1x3 pack e2",
          Kokkos::RangePolicy<>(0,nj),
          KOKKOS_LAMBDA (int idx_j){
            int j = idx_j*2+js;
            Real el1 = dx2f(j);
            Real el2 = dx2f(j+1);
            buf[p + idx_j] = (e2(k,j,i)*el1+e2(k,j+1,i)*el2)/(el1+el2);

          });
        p += nj;
        // x2x3 edge
      } else if (nb.eid>=8 && nb.eid<12) {
        int j, k;
        if ((nb.eid&1)==0) j=pmb->js;
        else j=pmb->je+1;
        bool pole = pco->IsPole(j);
        if ((nb.eid&2)==0) k=pmb->ks;
        else k=pmb->ke+1;
        // restrict and pack e1
        is = pmb->is; ie = pmb->ie;
        ni = (ie - is)/2+1;
        Kokkos::parallel_for(
          "LoadEMFBoundaryBufferToCoarser NEIGHBOR_EDGE 3D x2x3 pack e1",
          Kokkos::RangePolicy<>(0,ni),
          KOKKOS_LAMBDA (int idx_i){
            int i = idx_i*2+is;
            Real le1_i  = dx1f(i);
            Real le1_i1 = dx1f(i+1);
            buf[p + idx_i] =(e1(k,j,i)*le1_i+e1(k,j,i+1)*le1_i1)/(le1_i+le1_i1);
          });
        p += ni;

      }
    } else if (pmb->block_size.nx2 > 1) { // 2D
      // x1x2 edge
      int i, j;
      if ((nb.eid&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((nb.eid&2)==0) j=pmb->js;
      else j=pmb->je+1;
      // pack e3
      ks = pmb->ks;
      Kokkos::parallel_for(
        "LoadEMFBoundaryBufferToCoarser NEIGHBOR_EDGE 2D x1x2 pack e3",
        Kokkos::RangePolicy<>(0,1),
        KOKKOS_LAMBDA (int x){
          buf[p] = e3(ks,j,i);
        });
      p += 1;
      
    }
  }
  return p;
}

//----------------------------------------------------------------------------------------
//! \fn int BoundaryValues::LoadEMFBoundaryPolarBuffer(Real *buf,
//          const PolarNeighborBlock &nb)
//  \brief Load EMF values along polar axis into send buffers

int BoundaryValues::LoadEMFBoundaryPolarBuffer(Real *buf, const PolarNeighborBlock &nb) {
  MeshBlock *pmb = pmy_block_;
  int count = 0;
  int j = nb.north ? pmb->js : pmb->je+1;
  for (int i = pmb->is; i <= pmb->ie; ++i) {
    Real val = 0.0;
    for (int k = pmb->ks; k <= pmb->ke; ++k) {  // avoid double counting right ends
      val += pmb->pfield->e.x1e(k, j, i);
    }
    buf[count++] = val / (pmb->ke - pmb->ks + 1);
  }
  return count;
}

//----------------------------------------------------------------------------------------
//! \fn void BoundaryValues::SendEMFCorrection(void)
//  \brief Restrict, pack and send the surace EMF to the coarse neighbor(s) if
//  needed
void BoundaryValues::SendEMFCorrection(void) {
  MeshBlock *pmb=pmy_block_;

  // Send non-polar EMF values
  for (int n=0; n<nneighbor; n++) {
    NeighborBlock& nb = neighbor[n];
    if ((nb.type!=NEIGHBOR_FACE) && (nb.type!=NEIGHBOR_EDGE)) break;
    int p=0;
    if (nb.level==pmb->loc.level) {
      if ((nb.type==NEIGHBOR_FACE)
      || ((nb.type==NEIGHBOR_EDGE) && (edge_flag_[nb.eid]==true)))
        p=LoadEMFBoundaryBufferSameLevel(bd_emfcor_.Ksend[nb.bufid], nb);
      else continue;
    } else if (nb.level==pmb->loc.level-1) {
      p=LoadEMFBoundaryBufferToCoarser(bd_emfcor_.Ksend[nb.bufid], nb);
    } else {
      continue;
    }
    if (nb.rank==Globals::my_rank) { // on the same node
      MeshBlock *pbl=pmb->pmy_mesh->FindMeshBlock(nb.gid);
      Kokkos::deep_copy(pbl->pbval->bd_emfcor_.Krecv[nb.targetid],
                  bd_emfcor_.Ksend[nb.bufid]);
      pbl->pbval->bd_emfcor_.flag[nb.targetid]=BNDRY_ARRIVED;
    }
#ifdef MPI_PARALLEL
    else { //NOLINT
      Kokkos::fence();
      MPI_Start(&(bd_emfcor_.req_send[nb.bufid]));
    }
#endif
  }

  // Send polar EMF values
  for (int n = 0; n < num_north_polar_blocks_; ++n) {
    const PolarNeighborBlock &nb = polar_neighbor_north[n];
    int count = LoadEMFBoundaryPolarBuffer(emf_north_send_[n], nb);
    if (nb.rank == Globals::my_rank) { // on the same node
      MeshBlock *pbl = pmb->pmy_mesh->FindMeshBlock(nb.gid);
      std::memcpy(pbl->pbval->emf_north_recv_[pmb->loc.lx3],
          emf_north_send_[n], count * sizeof(Real));
      pbl->pbval->emf_north_flag_[pmb->loc.lx3] = BNDRY_ARRIVED;
    }
#ifdef MPI_PARALLEL
    else { // NOLINT
      Kokkos::fence();
      MPI_Start(&req_emf_north_send_[n]);
    }
#endif
  }
  for (int n = 0; n < num_south_polar_blocks_; ++n) {
    const PolarNeighborBlock &nb = polar_neighbor_south[n];
    int count = LoadEMFBoundaryPolarBuffer(emf_south_send_[n], nb);
    if (nb.rank == Globals::my_rank) { // on the same node
      MeshBlock *pbl = pmb->pmy_mesh->FindMeshBlock(nb.gid);
      std::memcpy(pbl->pbval->emf_south_recv_[pmb->loc.lx3],
          emf_south_send_[n], count * sizeof(Real));
      pbl->pbval->emf_south_flag_[pmb->loc.lx3] = BNDRY_ARRIVED;
    }
#ifdef MPI_PARALLEL
    else { // NOLINT
      Kokkos::fence();
      MPI_Start(&req_emf_south_send_[n]);
    }
#endif
  }
  return;
}


//----------------------------------------------------------------------------------------
//! \fn void BoundaryValues::SetEMFBoundarySameLevel(Kokkos::View<Real*,DevSpace>& buf,
//                                                   const NeighborBlock& nb)
//  \brief Add up the EMF received from a block on the same level
//         Later they will be divided in the AverageEMFBoundary function

void BoundaryValues::SetEMFBoundarySameLevel(Kokkos::View<Real*,DevSpace>& buf,
                                             const NeighborBlock& nb) {
  MeshBlock *pmb=pmy_block_;
  auto e1=pmb->pfield->e.x1e.get_KView3D();
  auto e2=pmb->pfield->e.x2e.get_KView3D();
  auto e3=pmb->pfield->e.x3e.get_KView3D();

  //Number of threads
  int ni, nj, nk;

  //Loop bounds
  int is, js, ks;
  int ie, je, ke;

  int p=0;
  if (nb.type==NEIGHBOR_FACE) {
    if (pmb->block_size.nx3 > 1) { // 3D
      // x1 direction
      if (nb.fid==INNER_X1 || nb.fid==OUTER_X1) {
        int i;
        if (nb.fid==INNER_X1) i=pmb->is;
        else i=pmb->ie+1;

        if (nb.shear and nb.fid==INNER_X1) {
          // store e2 for shearing periodic bcs
          for (int k=pmb->ks; k<=pmb->ke+1; k++) {
            for (int j=pmb->js; j<=pmb->je; j++)
              shboxvar_inner_emf_.x2e(k,j) += buf[p++];
          }
          // store e3 for shearing periodic bcs
          for (int k=pmb->ks; k<=pmb->ke; k++) {
            for (int j=pmb->js; j<=pmb->je+1; j++)
              shboxvar_inner_emf_.x3e(k,j) += buf[p++];
          }
        } else if (nb.shear and nb.fid==OUTER_X1) {
          // store e2 for shearing periodic bcs
          for (int k=pmb->ks; k<=pmb->ke+1; k++) {
            for (int j=pmb->js; j<=pmb->je; j++)
              shboxvar_outer_emf_.x2e(k,j) += buf[p++];
          }
          // store e3 for shearing periodic bcs
          for (int k=pmb->ks; k<=pmb->ke; k++) {
            for (int j=pmb->js; j<=pmb->je+1; j++)
              shboxvar_outer_emf_.x3e(k,j) += buf[p++];
          }
        } else {
          // unpack e2
          ks = pmb->ks; ke = pmb->ke+1; //Determine loop bounds
          js = pmb->js; je = pmb->je;
          nk = ke+1-ks; //Determine number of threads needed
          nj = je+1-js;
          Kokkos::parallel_for(
            "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x1 unpack e2",
            Kokkos::MDRangePolicy<Kokkos::Rank<2>>({ks,js},{ke+1,je+1}),
            KOKKOS_LAMBDA (int k, int j) {
              e2(k,j,i)+=buf[p+ (j-js + nj*(k-ks )) ]; //Add buf to e2
            });
          p += nj*nk; //offset p

          // unpack e3
          ks = pmb->ks; ke = pmb->ke;
          js = pmb->js; je = pmb->je+1;
          nk = ke+1-ks;
          nj = je+1-js;
          Kokkos::parallel_for(
            "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x1 unpack e3",
            Kokkos::MDRangePolicy<Kokkos::Rank<2>>({ks,js},{ke+1,je+1}),
            KOKKOS_LAMBDA (int k, int j) {
              e3(k,j,i)+=buf[p+ (j-js + nj*(k-ks )) ];
            });
          p += nj*nk; //offset p
        }
        // x2 direction
      } else if (nb.fid==INNER_X2 || nb.fid==OUTER_X2) {
        int j;
        if (nb.fid==INNER_X2) j=pmb->js;
        else j=pmb->je+1;
        // unpack e1
        Real sign = (nb.polar and flip_across_pole_field[IB1]) ? -1.0 : 1.0;
        ks = pmb->ks; ke = pmb->ke+1;
        is = pmb->is; ie = pmb->ie;
        nk = ke+1-ks;
        ni = ie+1-is;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x2 unpack e1",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({ks,is},{ke+1,ie+1}),
          KOKKOS_LAMBDA (int k, int i) {
            e1(k,j,i)+=sign*buf[p+ (i-is + ni*(k-ks )) ];
          });
        p += ni*nk; //offset p
        // unpack e3
        sign = (nb.polar and flip_across_pole_field[IB3]) ? -1.0 : 1.0;
        ks = pmb->ks; ke = pmb->ke;
        is = pmb->is; ie = pmb->ie+1;
        nk = ke+1-ks;
        ni = ie+1-is;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x2 unpack e3",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({ks,is},{ke+1,ie+1}),
          KOKKOS_LAMBDA (int k, int i) {
            e3(k,j,i) += sign*buf[p+ (i-is + ni*(k-ks )) ];
          });
        p += ni*nk; //offset p
        // x3 direction
      } else if (nb.fid==INNER_X3 || nb.fid==OUTER_X3) {
        int k;
        if (nb.fid==INNER_X3) k=pmb->ks;
        else k=pmb->ke+1;
        // unpack e1
        js = pmb->js; je = pmb->je+1;
        is = pmb->is; ie = pmb->ie;
        nj = je+1-js;
        ni = ie+1-is;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x3 unpack e1",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({js,is},{je+1,ie+1}),
          KOKKOS_LAMBDA (int j, int i) {
            e1(k,j,i)+=buf[p+ (i-is + ni*(j-js )) ];
          });
        p += ni*nj; //offset p
        // unpack e2
        js = pmb->js; je = pmb->je;
        is = pmb->is; ie = pmb->ie+1;
        nj = je+1-js;
        ni = ie+1-is;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 3D x3 unpack e2",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({js,is},{je+1,ie+1}),
          KOKKOS_LAMBDA (int j, int i) {
            e2(k,j,i)+=buf[p+ (i-is + ni*(j-js )) ];
          });
        p += ni*nj; //offset p
      }
    } else if (pmb->block_size.nx2 > 1) { // 2D
      int k=pmb->ks;
      // x1 direction
      if (nb.fid==INNER_X1 || nb.fid==OUTER_X1) {
        int i;
        if (nb.fid==INNER_X1) i=pmb->is;
        else i=pmb->ie+1;
        // unpack e2
        js = pmb->js; je = pmb->je;
        nj = je+1-js;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x1 unpack e2",
          Kokkos::RangePolicy<>(js,je+1),
          KOKKOS_LAMBDA (int j){
            e2(k+1,j,i) += buf[p + (j -js)];
            e2(k  ,j,i) += buf[p + (j -js)];
          });
        p += nj;

        // unpack e3
        js = pmb->js; je = pmb->je+1;
        nj = je+1-js;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x1 unpack e3",
          Kokkos::RangePolicy<>(js,je+1),
          KOKKOS_LAMBDA (int j){
            e3(k,j,i) += buf[p + (j -js)];
          });
        p += nj;

        // x2 direction
      } else if (nb.fid==INNER_X2 || nb.fid==OUTER_X2) {
        int j;
        if (nb.fid==INNER_X2) j=pmb->js;
        else j=pmb->je+1;
        // unpack e1
        is = pmb->is; ie = pmb->ie;
        ni = ie+1-is;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x2 unpack e1",
          Kokkos::RangePolicy<>(is,ie+1),
          KOKKOS_LAMBDA (int i){
            e1(k+1,j,i)+=buf[p + (i -is)];
            e1(k  ,j,i)+=buf[p + (i -is)];
          });
        p += ni;

        // unpack e3
        is = pmb->is; ie = pmb->ie+1;
        ni = ie+1-is;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x2 unpack e3",
          Kokkos::RangePolicy<>(is,ie+1),
          KOKKOS_LAMBDA (int i){
            e3(k,j,i)+=buf[p + (i -is)];
          });
        p += ni;
      }
    } else { // 1D
      int i, j=pmb->js, k=pmb->ks;
      if (nb.fid==INNER_X1) i=pmb->is;
      else i=pmb->ie+1;
      // unpack e2
      Kokkos::parallel_for("SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 1D",
        Kokkos::RangePolicy<>(0,1),
        KOKKOS_LAMBDA (int x){
         //unpack e2
         e2(k+1,j,i)+=buf[p + 0];
         e2(k  ,j,i)+=buf[p + 0];
         //unpack e3
         e3(k,j+1,i)+=buf[p + 1];
         e3(k  ,j,i)+=buf[p + 1];
        });
      p += 2;
    }
  } else if (nb.type==NEIGHBOR_EDGE) {
    // x1x2 edge (2D and 3D)
    if (nb.eid>=0 && nb.eid<4) {
      int i, j;
      if ((nb.eid&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((nb.eid&2)==0) j=pmb->js;
      else j=pmb->je+1;
      if (nb.shear and nb.ox1==-1) {
        // store e3 for shearing periodic bcs
        for (int k=pmb->ks; k<=pmb->ke; k++)
          shboxvar_inner_emf_.x3e(k,j) += buf[p++];
      } else if (nb.shear and nb.ox1==1) {
        // store e3 for shearing periodic bcs
        for (int k=pmb->ks; k<=pmb->ke; k++)
          shboxvar_outer_emf_.x3e(k,j) += buf[p++];
      } else {
      // unpack e3
        Real sign = (nb.polar and flip_across_pole_field[IB3]) ? -1.0 : 1.0;
        ks = pmb->ks; ke = pmb->ke;
        nk = ke+1-ks;
        Kokkos::parallel_for("SetEMFBoundaryBufferSameLevel NEIGHBOR_EDGE x1x2 unpack e3",
          Kokkos::RangePolicy<>(ks,ke+1),
          KOKKOS_LAMBDA (int k){
             e3(k,j,i) += sign*buf[p + (k -ks)];
          });
        p += nk;
      }
      // x1x3 edge
    } else if (nb.eid>=4 && nb.eid<8) {
      int i, k;
      if ((nb.eid&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((nb.eid&2)==0) k=pmb->ks;
      else k=pmb->ke+1;
      if (nb.shear and nb.ox1==-1) {
        // store e2 for shearing periodic bcs
        for (int j=pmb->js; j<=pmb->je; j++)
          shboxvar_inner_emf_.x2e(k,j) += buf[p++];
      } else if (nb.shear and nb.ox1==1) {
        // store e2 for shearing periodic bcs
        for (int j=pmb->js; j<=pmb->je; j++)
          shboxvar_outer_emf_.x2e(k,j) += buf[p++];
      } else {
      // unpack e2
        js = pmb->js; je = pmb->je;
        nj = je+1-js;
        Kokkos::parallel_for("SetEMFBoundaryBufferSameLevel NEIGHBOR_EDGE x1x3 unpack e2",
          Kokkos::RangePolicy<>(js,je+1),
          KOKKOS_LAMBDA (int j){
            e2(k,j,i)+=buf[p + (j -js)];
          });
        p += nj;
      }
      // x2x3 edge
    } else if (nb.eid>=8 && nb.eid<12) {
      int j, k;
      if ((nb.eid&1)==0) j=pmb->js;
      else j=pmb->je+1;
      if ((nb.eid&2)==0) k=pmb->ks;
      else k=pmb->ke+1;
      // unpack e1
      Real sign = (nb.polar and flip_across_pole_field[IB1]) ? -1.0 : 1.0;
      is = pmb->is; ie = pmb->ie;
      ni = ie+1-is;
      Kokkos::parallel_for("SetEMFBoundaryBufferSameLevel NEIGHBOR_EDGE x2x3 unpack e1",
        Kokkos::RangePolicy<>(is,ie+1),
        KOKKOS_LAMBDA (int i){
           e1(k,j,i)+=sign*buf[p + (i -is)];
        });
      p += ni;
      
    }
  }

  return;
}


//----------------------------------------------------------------------------------------
//! \fn void BoundaryValues::SetEMFBoundaryFromFiner(Kokkos::View<Real*,DevSpace>&  buf,
//                                                   const NeighborBlock& nb)
//  \brief Add up the EMF received from a block on the finer level
//         Later they will be divided in the AverageEMFBoundary function

void BoundaryValues::SetEMFBoundaryFromFiner(Kokkos::View<Real*,DevSpace>& buf,
                                             const NeighborBlock& nb) {
  MeshBlock *pmb=pmy_block_;
  auto e1=pmb->pfield->e.x1e.get_KView3D();
  auto e2=pmb->pfield->e.x2e.get_KView3D();
  auto e3=pmb->pfield->e.x3e.get_KView3D();

  //Number of threads in each dimension
  int ni,nj,nk;

  int p=0;
  if (nb.type==NEIGHBOR_FACE) {
    if (pmb->block_size.nx3 > 1) { // 3D
      // x1 direction
      if (nb.fid==INNER_X1 || nb.fid==OUTER_X1) {
        int i, jl=pmb->js, ju=pmb->je, kl=pmb->ks, ku=pmb->ke;
        if (nb.fid==INNER_X1) i=pmb->is;
        else i=pmb->ie+1;
        if (nb.fi1==0) ju=pmb->js+pmb->block_size.nx2/2-1;
        else jl=pmb->js+pmb->block_size.nx2/2;
        if (nb.fi2==0) ku=pmb->ks+pmb->block_size.nx3/2-1;
        else kl=pmb->ks+pmb->block_size.nx3/2;
        // unpack e2
        nk = ku+1+1-kl; //Determine number of threads needed
        nj = ju+1-jl;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 3D x1 unpack e2",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({kl,jl},{ku+1+1,ju+1}),
          KOKKOS_LAMBDA (int k, int j) {
            e2(k,j,i)+=buf[p+ (j-jl + nj*(k-kl )) ]; //Add e2 to buf
          });
        p += nj*nk; //offset p
        // unpack e3
        nk = ku+1-kl;
        nj = ju+1+1-jl;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 3D x1 unpack e3",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({kl,jl},{ku+1,ju+1+1}),
          KOKKOS_LAMBDA (int k, int j) {
            e3(k,j,i)+=buf[p+ (j-jl + nj*(k-kl )) ];
          });
        p += nj*nk; //offset p
        // x2 direction
      } else if (nb.fid==INNER_X2 || nb.fid==OUTER_X2) {
        int j, il=pmb->is, iu=pmb->ie, kl=pmb->ks, ku=pmb->ke;
        if (nb.fid==INNER_X2) j=pmb->js;
        else j=pmb->je+1;
        if (nb.fi1==0) iu=pmb->is+pmb->block_size.nx1/2-1;
        else il=pmb->is+pmb->block_size.nx1/2;
        if (nb.fi2==0) ku=pmb->ks+pmb->block_size.nx3/2-1;
        else kl=pmb->ks+pmb->block_size.nx3/2;
        // unpack e1
        Real sign = (nb.polar and flip_across_pole_field[IB1]) ? -1.0 : 1.0;
        nk = ku+1+1-kl;
        ni = iu+1-il;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 3D x2 unpack e1",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({kl,il},{ku+1+1,iu+1}),
          KOKKOS_LAMBDA (int k, int i) {
            e1(k,j,i)+=sign*buf[p+ (i-il + ni*(k-kl )) ];
          });
        p += ni*nk; //offset p
        // unpack e3
        sign = (nb.polar and flip_across_pole_field[IB3]) ? -1.0 : 1.0;
        nk = ku+1-kl;
        ni = iu+1+1-il;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 3D x2 unpack e3",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({kl,il},{ku+1,iu+1+1}),
          KOKKOS_LAMBDA (int k, int i) {
            e3(k,j,i) += sign*buf[p+ (i-il + ni*(k-kl )) ];
          });
        p += ni*nk; //offset p
      // x3 direction
      } else if (nb.fid==INNER_X3 || nb.fid==OUTER_X3) {
        int k, il=pmb->is, iu=pmb->ie, jl=pmb->js, ju=pmb->je;
        if (nb.fid==INNER_X3) k=pmb->ks;
        else k=pmb->ke+1;
        if (nb.fi1==0) iu=pmb->is+pmb->block_size.nx1/2-1;
        else il=pmb->is+pmb->block_size.nx1/2;
        if (nb.fi2==0) ju=pmb->js+pmb->block_size.nx2/2-1;
        else jl=pmb->js+pmb->block_size.nx2/2;
        // unpack e1
        nj = ju+1+1-jl;
        ni = iu+1-il;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 3D x3 unpack e1",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({jl,il},{ju+1+1,iu+1}),
          KOKKOS_LAMBDA (int j, int i) {
            e1(k,j,i)+=buf[p+ (i-il + ni*(j-jl )) ];
          });
        p += ni*nj; //offset p
        // unpack e2
        nj = ju+1-jl;
        ni = iu+1+1-il;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 3D x3 unpack e2",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({jl,il},{ju+1,iu+1+1}),
          KOKKOS_LAMBDA (int j, int i) {
            e2(k,j,i)+=buf[p+ (i-il + ni*(j-jl )) ];
          });
        p += ni*nj; //offset p
      }
    } else if (pmb->block_size.nx2 > 1) { // 2D
      int k=pmb->ks;
      // x1 direction
      if (nb.fid==INNER_X1 || nb.fid==OUTER_X1) {
        int i, jl=pmb->js, ju=pmb->je;
        if (nb.fid==INNER_X1) i=pmb->is;
        else i=pmb->ie+1;
        if (nb.fi1==0) ju=pmb->js+pmb->block_size.nx2/2-1;
        else jl=pmb->js+pmb->block_size.nx2/2;
        // unpack e2
        nj = ju+1-jl;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 2D x1 unpack e2",
          Kokkos::RangePolicy<>(jl,ju+1),
          KOKKOS_LAMBDA (int j){
            e2(k+1,j,i) += buf[p + (j -jl)];
            e2(k  ,j,i) += buf[p + (j -jl)];
          });
        p += nj;
        // unpack e3
        nj = ju+1+1-jl;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 2D x1 unpack e3",
          Kokkos::RangePolicy<>(jl,ju+1+1),
          KOKKOS_LAMBDA (int j){
            e3(k,j,i) += buf[p + (j -jl)];
          });
        p += nj;
        // x2 direction
      } else if (nb.fid==INNER_X2 || nb.fid==OUTER_X2) {
        int j, il=pmb->is, iu=pmb->ie;
        if (nb.fid==INNER_X2) j=pmb->js;
        else j=pmb->je+1;
        if (nb.fi1==0) iu=pmb->is+pmb->block_size.nx1/2-1;
        else il=pmb->is+pmb->block_size.nx1/2;
        // unpack e1
        ni = iu+1-il;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 2D x2 unpack e1",
          Kokkos::RangePolicy<>(il,iu+1),
          KOKKOS_LAMBDA (int i){
            e1(k+1,j,i)+=buf[p + (i -il)];
            e1(k  ,j,i)+=buf[p + (i -il)];
          });
        p += ni;
        // unpack e3
        ni = iu+1+1-il;
        Kokkos::parallel_for(
          "SetEMFBoundaryBufferSameLevel NEIGHBOR_FACE 2D x2 unpack e3",
          Kokkos::RangePolicy<>(il,iu+1+1),
          KOKKOS_LAMBDA (int i){
            e3(k,j,i)+=buf[p + (i -il)];
          });
        p += ni;
      }
    } else { // 1D
      int i, j=pmb->js, k=pmb->ks;
      if (nb.fid==INNER_X1) i=pmb->is;
      else i=pmb->ie+1;
      Kokkos::parallel_for("SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 1D",
        Kokkos::RangePolicy<>(0,1),
        KOKKOS_LAMBDA (int x){
         //unpack e2
         e2(k+1,j,i)+=buf[p + 0];
         e2(k  ,j,i)+=buf[p + 0];
         //unpack e3
         e3(k,j+1,i)+=buf[p + 1];
         e3(k  ,j,i)+=buf[p + 1];
        });
      p += 2;
    }
  } else if (nb.type==NEIGHBOR_EDGE) {
    if (pmb->block_size.nx3 > 1) { // 3D
      // x1x2 edge
      if (nb.eid>=0 && nb.eid<4) {
        int i, j, kl=pmb->ks, ku=pmb->ke;
        if ((nb.eid&1)==0) i=pmb->is;
        else i=pmb->ie+1;
        if ((nb.eid&2)==0) j=pmb->js;
        else j=pmb->je+1;
        if (nb.fi1==0) ku=pmb->ks+pmb->block_size.nx3/2-1;
        else kl=pmb->ks+pmb->block_size.nx3/2;
        // unpack e3
        Real sign = (nb.polar and flip_across_pole_field[IB3]) ? -1.0 : 1.0;
        nk = ku+1-kl;
        Kokkos::parallel_for("SetEMFBoundaryBufferFromFiner NEIGHBOR_EDGE x1x2 unpack e3",
          Kokkos::RangePolicy<>(kl,ku+1),
          KOKKOS_LAMBDA (int k){
             e3(k,j,i) += sign*buf[p + (k -kl)];
          });
        p += nk;
        // x1x3 edge
      } else if (nb.eid>=4 && nb.eid<8) {
        int i, k, jl=pmb->js, ju=pmb->je;
        if ((nb.eid&1)==0) i=pmb->is;
        else i=pmb->ie+1;
        if ((nb.eid&2)==0) k=pmb->ks;
        else k=pmb->ke+1;
        if (nb.fi1==0) ju=pmb->js+pmb->block_size.nx2/2-1;
        else jl=pmb->js+pmb->block_size.nx2/2;
        // unpack e2
        nj = ju+1-jl;
        Kokkos::parallel_for("SetEMFBoundaryBufferFromFiner NEIGHBOR_EDGE x1x3 unpack e2",
          Kokkos::RangePolicy<>(jl,ju+1),
          KOKKOS_LAMBDA (int j){
            e2(k,j,i)+=buf[p + (j -jl)];
          });
        p += nj;
        // x2x3 edge
      } else if (nb.eid>=8 && nb.eid<12) {
        int j, k, il=pmb->is, iu=pmb->ie;
        if ((nb.eid&1)==0) j=pmb->js;
        else j=pmb->je+1;
        if ((nb.eid&2)==0) k=pmb->ks;
        else k=pmb->ke+1;
        if (nb.fi1==0) iu=pmb->is+pmb->block_size.nx1/2-1;
        else il=pmb->is+pmb->block_size.nx1/2;
        // unpack e1
        Real sign = (nb.polar and flip_across_pole_field[IB1]) ? -1.0 : 1.0;
        ni = iu+1-il;
        Kokkos::parallel_for("SetEMFBoundaryBufferFromFiner NEIGHBOR_EDGE x2x3 unpack e1",
          Kokkos::RangePolicy<>(il,iu+1),
          KOKKOS_LAMBDA (int i){
             e1(k,j,i)+=sign*buf[p + (i -il)];
          });
        p += ni;
      }
    } else if (pmb->block_size.nx2 > 1) { // 2D
      int i, j, k=pmb->ks;
      if ((nb.eid&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((nb.eid&2)==0) j=pmb->js;
      else j=pmb->je+1;
      //unpack e3
      Kokkos::parallel_for("SetEMFBoundaryBufferFromFiner NEIGHBOR_FACE 1D",
        Kokkos::RangePolicy<>(0,1),
        KOKKOS_LAMBDA (int x){
         e3(k,j,i)+=buf[p];
        });
      p += 1;
    }
  }

  return;
}

//----------------------------------------------------------------------------------------
//! \fn void BoundaryValues::SetEMFBoundaryPolar(Real **buf_list, int num_bufs,
//          bool north)
//  \brief Overwrite EMF values along polar axis with azimuthal averages

void BoundaryValues::SetEMFBoundaryPolar(Real **buf_list, int num_bufs, bool north) {
  MeshBlock *pmb = pmy_block_;
  if (pmb->block_size.nx3 > 1) {
    int j = north ? pmb->js : pmb->je+1;
    int count = 0;
    for (int i = pmb->is; i <= pmb->ie; ++i) {
      Real val = 0.0;
      for (int n = 0; n < num_bufs; ++n)
        val += buf_list[n][count];
      for (int k = pmb->ks-NGHOST; k <= pmb->ke+NGHOST+1; ++k)
        pmb->pfield->e.x1e(k, j, i) = val / num_bufs;
      ++count;
    }
  }
  return;
}

//----------------------------------------------------------------------------------------
//! \fn void BoundaryValues::ClearCoarseEMFBoundary(void)
//  \brief Clear the EMFs on the surface/edge contacting with a finer block

void BoundaryValues::ClearCoarseEMFBoundary(void) {
  MeshBlock *pmb=pmy_block_;
  auto e1 = pmb->pfield->e.x1e.get_KView3D();
  auto e2 = pmb->pfield->e.x2e.get_KView3D();
  auto e3 = pmb->pfield->e.x3e.get_KView3D();

  int i, j, k, nl;
  // face
  for (int n=0; n<nface_; n++) {
    if (n==INNER_X1 || n==OUTER_X1) {
      if (n==INNER_X1) i=pmb->is;
      else i=pmb->ie+1;
      nl=nblevel[1][1][2*n];
      if (nl>pmb->loc.level) { // finer
        if (pmb->block_size.nx3 > 1) { // 3D
          for (int k=pmb->ks+1; k<=pmb->ke; k++) {
            for (int j=pmb->js; j<=pmb->je; j++)
              e2(k,j,i)=0.0;
          }
          for (int k=pmb->ks; k<=pmb->ke; k++) {
            for (int j=pmb->js+1; j<=pmb->je; j++)
              e3(k,j,i)=0.0;
          }
        } else if (pmb->block_size.nx2 > 1) { // 2D
          for (int j=pmb->js; j<=pmb->je; j++)
            e2(pmb->ks,j,i)=e2(pmb->ks+1,j,i)=0.0;
          for (int j=pmb->js+1; j<=pmb->je; j++)
            e3(pmb->ks,j,i)=0.0;
        } else { // 1D
          e2(pmb->ks,pmb->js,i)=e2(pmb->ks+1,pmb->js,i)=0.0;
          e3(pmb->ks,pmb->js,i)=e3(pmb->ks,pmb->js+1,i)=0.0;
        }
      }
    }
    if (n==INNER_X2 || n==OUTER_X2) {
      if (n==INNER_X2) j=pmb->js;
      else j=pmb->je+1;
      nl=nblevel[1][2*n-4][1];
      if (nl>pmb->loc.level) { // finer
        if (pmb->block_size.nx3 > 1) { // 3D
          for (int k=pmb->ks+1; k<=pmb->ke; k++) {
            for (int i=pmb->is; i<=pmb->ie; i++)
              e1(k,j,i)=0.0;
          }
          for (int k=pmb->ks; k<=pmb->ke; k++) {
            for (int i=pmb->is+1; i<=pmb->ie; i++)
              e3(k,j,i)=0.0;
          }
        } else if (pmb->block_size.nx2 > 1) { // 2D
          for (int i=pmb->is; i<=pmb->ie; i++)
            e1(pmb->ks,j,i)=e1(pmb->ks+1,j,i)=0.0;
          for (int i=pmb->is+1; i<=pmb->ie; i++)
            e3(pmb->ks,j,i)=0.0;
        }
      }
    }
    if (n==INNER_X3 || n==OUTER_X3) {
      if (n==INNER_X3) k=pmb->ks;
      else k=pmb->ke+1;
      nl=nblevel[2*n-8][1][1];
      if (nl>pmb->loc.level) { // finer
        // this is always 3D
        for (int j=pmb->js+1; j<=pmb->je; j++) {
          for (int i=pmb->is; i<=pmb->ie; i++)
            e1(k,j,i)=0.0;
        }
        for (int j=pmb->js; j<=pmb->je; j++) {
          for (int i=pmb->is+1; i<=pmb->ie; i++)
            e2(k,j,i)=0.0;
        }
      }
    }
  }
  // edge
  for (int n=0; n<nedge_; n++) {
    if (edge_flag_[n]==true) continue;
    if (n>=0 && n<4) {
      if ((n&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((n&2)==0) j=pmb->js;
      else j=pmb->je+1;
      for (int k=pmb->ks; k<=pmb->ke; k++)
        e3(k,j,i)=0.0;
      // x1x3 edge
    } else if (n>=4 && n<8) {
      if ((n&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((n&2)==0) k=pmb->ks;
      else k=pmb->ke+1;
      for (int j=pmb->js; j<=pmb->je; j++)
        e2(k,j,i)=0.0;
      // x2x3 edge
    } else if (n>=8 && n<12) {
      if ((n&1)==0) j=pmb->js;
      else j=pmb->je+1;
      if ((n&2)==0) k=pmb->ks;
      else k=pmb->ke+1;
      for (int i=pmb->is; i<=pmb->ie; i++)
        e1(k,j,i)=0.0;
    }
  }
  return;
}


//----------------------------------------------------------------------------------------
//! \fn void BoundaryValues::AverageEMFBoundary(void)
//  \brief Set EMF boundary received from a block on the finer level

void BoundaryValues::AverageEMFBoundary(void) {
  MeshBlock *pmb=pmy_block_;

  auto e1 = pmb->pfield->e.x1e.get_KView3D();
  auto e2 = pmb->pfield->e.x2e.get_KView3D();
  auto e3 = pmb->pfield->e.x3e.get_KView3D();

  int i, j, k, nl;
  int ks = pmb->ks;

  // face
  for (int n=0; n<nface_; n++) {
    if ((block_bcs[n] != BLOCK_BNDRY) && (block_bcs[n] != PERIODIC_BNDRY)
        && (block_bcs[n] != POLAR_BNDRY)) continue;
    if (n==INNER_X1 || n==OUTER_X1) {
      if (n==INNER_X1) i=pmb->is;
      else i=pmb->ie+1;
      nl=nblevel[1][1][2*n];
      if (nl==pmb->loc.level) { // same ; divide all the face EMFs by 2
        if (pmb->block_size.nx3 > 1) { // 3D
          Kokkos::parallel_for("AverageEMFBoundary same lvl 3D e2",
            Kokkos::MDRangePolicy<Kokkos::Rank<2>>({pmb->ks+1,pmb->js},
                                                   {pmb->ke+1,pmb->je+1}),
            KOKKOS_LAMBDA (int k, int j) {
              e2(k,j,i)*=0.5;
            });
          Kokkos::parallel_for("AverageEMFBoundary same lvl 3D e3",
            Kokkos::MDRangePolicy<Kokkos::Rank<2>>({pmb->ks,pmb->js+1},
                                                   {pmb->ke+1,pmb->je+1}),
            KOKKOS_LAMBDA (int k, int j) {
              e3(k,j,i)*=0.5;
            });
        } else if (pmb->block_size.nx2 > 1) { // 2D
          Kokkos::parallel_for("AverageEMFBoundary same lvl 2D e2",
            Kokkos::RangePolicy<>(pmb->js,pmb->je+1),
            KOKKOS_LAMBDA (int j) {
              e2(ks,j,i)*=0.5;
              e2(ks+1,j,i)*=0.5;
            });
          Kokkos::parallel_for("AverageEMFBoundary same lvl 2D e3",
            Kokkos::RangePolicy<>(pmb->js+1,pmb->je+1),
            KOKKOS_LAMBDA (int j) {
              e3(ks,j,i)*=0.5;
            });
        } else { // 1D
          Kokkos::parallel_for("AverageEMFBoundary same lvl 1D e2/e3",
            Kokkos::RangePolicy<>(pmb->js,pmb->js+1),
            KOKKOS_LAMBDA (int j) {
              e2(ks,j,i)*=0.5;
              e2(ks+1,j,i)*=0.5;
              e3(ks,j,i)*=0.5;
              e3(ks,j+1,i)*=0.5;
            });
        }
      } else if (nl>pmb->loc.level) { // finer; divide the overlapping EMFs by 2
        if (pmb->block_size.nx3 > 1) { // 3D
          int k=pmb->ks+pmb->block_size.nx3/2;
          Kokkos::parallel_for("AverageEMFBoundary finer lvl 3D e2",
            Kokkos::RangePolicy<>(pmb->js,pmb->je+1),
            KOKKOS_LAMBDA (int j) {
              e2(k,j,i)*=0.5;
            });
        }
        if (pmb->block_size.nx2 > 1) { // 2D or 3D
          int j=pmb->js+pmb->block_size.nx2/2;
          Kokkos::parallel_for("AverageEMFBoundary finer lvl 2D/3D e3",
            Kokkos::RangePolicy<>(pmb->ks,pmb->ke+1),
            KOKKOS_LAMBDA (int k) {
              e3(k,j,i)*=0.5;
            });
        }
      }
    }
    if (n==INNER_X2 || n==OUTER_X2) {
      if (n==INNER_X2) j=pmb->js;
      else j=pmb->je+1;
      nl=nblevel[1][2*n-4][1];
      if (nl==pmb->loc.level) { // same ; divide all the face EMFs by 2
        if (pmb->block_size.nx3 > 1) {
          Kokkos::parallel_for("AverageEMFBoundary same lvl e1",
            Kokkos::MDRangePolicy<Kokkos::Rank<2>>({pmb->ks+1,pmb->is},
                                                   {pmb->ke+1,pmb->ie+1}),
            KOKKOS_LAMBDA (int k, int i) {
              e1(k,j,i)*=0.5;
            });
          Kokkos::parallel_for("AverageEMFBoundary same lvl e3",
            Kokkos::MDRangePolicy<Kokkos::Rank<2>>({pmb->ks,pmb->is+1},
                                                   {pmb->ke+1,pmb->ie+1}),
            KOKKOS_LAMBDA (int k, int i) {
              e3(k,j,i)*=0.5;
            });
        } else if (pmb->block_size.nx2 > 1) {
          Kokkos::parallel_for("AverageEMFBoundary same lvl 2D e1",
            Kokkos::RangePolicy<>(pmb->is,pmb->ie+1),
            KOKKOS_LAMBDA (int i) {
              e1(ks,j,i)*=0.5;
              e1(ks+1,j,i)*=0.5;
            });
          Kokkos::parallel_for("AverageEMFBoundary same lvl 2D e3",
            Kokkos::RangePolicy<>(pmb->is+1,pmb->ie+1),
            KOKKOS_LAMBDA (int i) {
              e3(ks,j,i)*=0.5;
            });
        }
      } else if (nl>pmb->loc.level) { // finer; divide the overlapping EMFs by 2
        if (pmb->block_size.nx3 > 1) { // 3D
          int k=pmb->ks+pmb->block_size.nx3/2;
          Kokkos::parallel_for("AverageEMFBoundary finer lvl 2D e1",
            Kokkos::RangePolicy<>(pmb->is,pmb->ie+1),
            KOKKOS_LAMBDA (int i) {
              e1(k,j,i)*=0.5;
            });
        }
        if (pmb->block_size.nx2 > 1) { // 2D or 3D
          int i=pmb->is+pmb->block_size.nx1/2;
          Kokkos::parallel_for("AverageEMFBoundary finer lvl 2D e3",
            Kokkos::RangePolicy<>(pmb->ks,pmb->ke+1),
            KOKKOS_LAMBDA (int k) {
              e3(k,j,i)*=0.5;
            });
        }
      }
    }
    if (n==INNER_X3 || n==OUTER_X3) {
      if (n==INNER_X3) k=pmb->ks;
      else k=pmb->ke+1;
      nl=nblevel[2*n-8][1][1];
      if (nl==pmb->loc.level) { // same ; divide all the face EMFs by 2
        Kokkos::parallel_for("AverageEMFBoundary same lvl e3",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({pmb->js+1,pmb->is},
                                                 {pmb->je+1,pmb->ie+1}),
          KOKKOS_LAMBDA (int j, int i) {
            e1(k,j,i)*=0.5;
          });
        Kokkos::parallel_for("AverageEMFBoundary same lvl e2",
          Kokkos::MDRangePolicy<Kokkos::Rank<2>>({pmb->js,pmb->is+1},
                                                 {pmb->je+1,pmb->ie+1}),
          KOKKOS_LAMBDA (int j, int i) {
            e2(k,j,i)*=0.5;
          });
      } else if (nl>pmb->loc.level) { // finer; divide the overlapping EMFs by 2
        // this is always 3D
        int j=pmb->js+pmb->block_size.nx2/2;
        Kokkos::parallel_for("AverageEMFBoundary finer lvl 2D e1",
          Kokkos::RangePolicy<>(pmb->is,pmb->ie+1),
          KOKKOS_LAMBDA (int i) {
            e1(k,j,i)*=0.5;
          });
        int i=pmb->is+pmb->block_size.nx1/2;
        Kokkos::parallel_for("AverageEMFBoundary finer lvl 2D e2",
          Kokkos::RangePolicy<>(pmb->js,pmb->je+1),
          KOKKOS_LAMBDA (int j) {
            e2(k,j,i)*=0.5;
          });
      }
    }
  }
  // edge
  for (int n=0; n<nedge_; n++) {
    if (nedge_fine_[n]==1) continue;
    Real div=1.0/static_cast<Real>(nedge_fine_[n]);
    NeighborBlock& nb=neighbor[n+6];
    Real half_div=div;
    if (nb.shear) half_div=0.5;
    // x1x2 edge (both 2D and 3D)
    if (n>=0 && n<4) {
      if ((n&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((n&2)==0) j=pmb->js;
      else j=pmb->je+1;
      Kokkos::parallel_for("AverageEMFBoundary edge x1x2 e3",
        Kokkos::RangePolicy<>(pmb->ks,pmb->ke+1),
        KOKKOS_LAMBDA (int k) {
          e3(k,j,i)*=half_div;
        });
      // x1x3 edge
    } else if (n>=4 && n<8) {
      if ((n&1)==0) i=pmb->is;
      else i=pmb->ie+1;
      if ((n&2)==0) k=pmb->ks;
      else k=pmb->ke+1;
      Kokkos::parallel_for("AverageEMFBoundary edge x1x3 e2",
        Kokkos::RangePolicy<>(pmb->js,pmb->je+1),
        KOKKOS_LAMBDA (int j) {
          e2(k,j,i)*=half_div;
        });
      // x2x3 edge
    } else if (n>=8 && n<12) {
      if ((n&1)==0) j=pmb->js;
      else j=pmb->je+1;
      if ((n&2)==0) k=pmb->ks;
      else k=pmb->ke+1;
      Kokkos::parallel_for("AverageEMFBoundary edge x2x3 e1",
        Kokkos::RangePolicy<>(pmb->is,pmb->ie+1),
        KOKKOS_LAMBDA (int i) {
          e1(k,j,i)*=div;
        });
    }
  }
  return;
}

//----------------------------------------------------------------------------------------
//! \fn void BoundaryValues::PolarSingleEMF(void)
//  \brief single CPU in the azimuthal direction for the polar boundary

void BoundaryValues::PolarSingleEMF(void) {
  MeshBlock *pmb=pmy_block_;
  AthenaArray<Real> &e1=pmb->pfield->e.x1e;
  AthenaArray<Real> &e3=pmb->pfield->e.x3e;
  int j;

  if (pmb->loc.level == pmb->pmy_mesh->root_level && pmb->pmy_mesh->nrbx3 == 1
  && pmb->block_size.nx3 > 1) {
    if (block_bcs[INNER_X2]==POLAR_BNDRY||block_bcs[INNER_X2]==POLAR_BNDRY_WEDGE) {
      j=pmb->js;
      int nx3_half = (pmb->ke - pmb->ks + 1) / 2;
      for (int i=pmb->is; i<=pmb->ie; i++) {
        Real tote1=0.0;
        for (int k=pmb->ks; k<=pmb->ke; k++)
          tote1+=e1(k,j,i);
        Real e1a=tote1/static_cast<double>(pmb->ke-pmb->ks+1);
        for (int k=pmb->ks; k<=pmb->ke+1; k++)
          e1(k,j,i)=e1a;
      }
      for (int i=pmb->is; i<=pmb->ie+1; i++) {
        for (int k=pmb->ks; k<=pmb->ke; k++)
          exc_(k)=e3(k,j,i);
        for (int k=pmb->ks; k<=pmb->ke; k++) {
          int k_shift = k;
          k_shift += (k < (nx3_half+NGHOST) ? 1 : -1) * nx3_half;
          e3(k,j,i)=exc_(k_shift);
        }
      }
    }

    if (block_bcs[OUTER_X2]==POLAR_BNDRY||block_bcs[OUTER_X2]==POLAR_BNDRY_WEDGE) {
      j=pmb->je+1;
      int nx3_half = (pmb->ke - pmb->ks + 1) / 2;
      for (int i=pmb->is; i<=pmb->ie; i++) {
        Real tote1=0.0;
        for (int k=pmb->ks; k<=pmb->ke; ++k)
          tote1+=e1(k,j,i);
        Real e1a=tote1/static_cast<double>(pmb->ke-pmb->ks+1);
        for (int k=pmb->ks; k<=pmb->ke+1; ++k)
          e1(k,j,i)=e1a;
      }
      for (int i=pmb->is; i<=pmb->ie+1; i++) {
        for (int k=pmb->ks; k<=pmb->ke; k++)
          exc_(k)=e3(k,j,i);
        for (int k=pmb->ks; k<=pmb->ke; k++) {
          int k_shift = k;
          k_shift += (k < (nx3_half+NGHOST) ? 1 : -1) * nx3_half;
          e3(k,j,i)=exc_(k_shift);
        }
      }
    }
  }
  return;
}



//----------------------------------------------------------------------------------------
//! \fn void BoundaryValues::ReceiveEMFCorrection(void)
//  \brief Receive and Apply the surace EMF to the coarse neighbor(s) if needed

bool BoundaryValues::ReceiveEMFCorrection(void) {
  MeshBlock *pmb=pmy_block_;
  bool flag=true;

  // Receive same-level non-polar EMF values
  if (firsttime_==true) {
    for (int n=0; n<nneighbor; n++) { // first correct the same level
      NeighborBlock& nb = neighbor[n];
      if (nb.type!=NEIGHBOR_FACE && nb.type!=NEIGHBOR_EDGE) break;
      if (nb.level!=pmb->loc.level) continue;
      if ((nb.type==NEIGHBOR_FACE) || ((nb.type==NEIGHBOR_EDGE) &&
                                       (edge_flag_[nb.eid]==true))) {
        if (bd_emfcor_.flag[nb.bufid]==BNDRY_COMPLETED) continue;
        if (bd_emfcor_.flag[nb.bufid]==BNDRY_WAITING) {
          if (nb.rank==Globals::my_rank) { // on the same process
            flag=false;
            continue;
#ifdef MPI_PARALLEL
          } else { // MPI boundary
            int test;
            // no fence required due to receiving call
            MPI_Iprobe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&test,MPI_STATUS_IGNORE);
            MPI_Test(&(bd_emfcor_.req_recv[nb.bufid]),&test,MPI_STATUS_IGNORE);
            if (static_cast<bool>(test)==false) {
              flag=false;
              continue;
            }
            bd_emfcor_.flag[nb.bufid] = BNDRY_ARRIVED;
          }
#else
          }
#endif
        }
        // boundary arrived; apply EMF correction
        SetEMFBoundarySameLevel(bd_emfcor_.Krecv[nb.bufid], nb);
        bd_emfcor_.flag[nb.bufid] = BNDRY_COMPLETED;
      }
    }

    if (flag==false) return flag;
    if (pmb->pmy_mesh->multilevel==true)
      ClearCoarseEMFBoundary();
    firsttime_=false;
  }

  // Receive finer non-polar EMF values
  if (pmb->pmy_mesh->multilevel==true) {
    for (int n=0; n<nneighbor; n++) { // then from finer
      NeighborBlock& nb = neighbor[n];
      if (nb.type!=NEIGHBOR_FACE && nb.type!=NEIGHBOR_EDGE) break;
      if (nb.level!=pmb->loc.level+1) continue;
      if (bd_emfcor_.flag[nb.bufid]==BNDRY_COMPLETED) continue;
      if (bd_emfcor_.flag[nb.bufid]==BNDRY_WAITING) {
        if (nb.rank==Globals::my_rank) {// on the same process
          flag=false;
          continue;
#ifdef MPI_PARALLEL
        } else { // MPI boundary
          int test;
          // no fence required due to receiving call
          MPI_Iprobe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&test,MPI_STATUS_IGNORE);
          MPI_Test(&(bd_emfcor_.req_recv[nb.bufid]),&test,MPI_STATUS_IGNORE);
          if (static_cast<bool>(test)==false) {
            flag=false;
            continue;
          }
          bd_emfcor_.flag[nb.bufid] = BNDRY_ARRIVED;
        }
#else
        }
#endif
      }
      // boundary arrived; apply EMF correction
      SetEMFBoundaryFromFiner(bd_emfcor_.Krecv[nb.bufid], nb);
      bd_emfcor_.flag[nb.bufid] = BNDRY_COMPLETED;
    }
  }

  // Receive polar EMF values
  for (int n = 0; n < num_north_polar_blocks_; ++n) {
    const PolarNeighborBlock &nb = polar_neighbor_north[n];
    if (emf_north_flag_[n] == BNDRY_WAITING) {
      if (nb.rank == Globals::my_rank) { // on the same process
        flag = false;
        continue;
#ifdef MPI_PARALLEL
      } else {
        int recv_flag;
        // no fence required due to receiving call
        MPI_Test(&req_emf_north_recv_[n], &recv_flag, MPI_STATUS_IGNORE);
        if (not recv_flag) {
          flag = false;
          continue;
        }
        emf_north_flag_[n] = BNDRY_ARRIVED;
      }
#else
      }
#endif
    }
  }
  for (int n = 0; n < num_south_polar_blocks_; ++n) {
    const PolarNeighborBlock &nb = polar_neighbor_south[n];
    if (emf_south_flag_[n] == BNDRY_WAITING) {
      if (nb.rank == Globals::my_rank) { // on the same process
        flag = false;
        continue;
#ifdef MPI_PARALLEL
      } else {
        int recv_flag;
        // no fence required due to receiving call
        MPI_Test(&req_emf_south_recv_[n], &recv_flag, MPI_STATUS_IGNORE);
        if (not recv_flag) {
          flag = false;
          continue;
        }
        emf_south_flag_[n] = BNDRY_ARRIVED;
      }
#else
      }
#endif
    }
  }

  if (flag==true) {
    AverageEMFBoundary();
    if (num_north_polar_blocks_ > 0)
      SetEMFBoundaryPolar(emf_north_recv_, num_north_polar_blocks_, true);
    for (int n = 0; n < num_north_polar_blocks_; ++n)
      emf_north_flag_[n] = BNDRY_COMPLETED;
    if (num_south_polar_blocks_ > 0)
      SetEMFBoundaryPolar(emf_south_recv_, num_south_polar_blocks_, false);
    for (int n = 0; n < num_south_polar_blocks_; ++n)
      emf_south_flag_[n] = BNDRY_COMPLETED;
    if (block_bcs[INNER_X2]==POLAR_BNDRY||block_bcs[OUTER_X2]==POLAR_BNDRY||
        block_bcs[INNER_X2]==POLAR_BNDRY_WEDGE||block_bcs[OUTER_X2]==POLAR_BNDRY_WEDGE)
      PolarSingleEMF();
  }
  return flag;
}

#ifndef EOS_ADIABATIC_MHD_HPP_
#define EOS_ADIABATIC_MHD_HPP_
//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================

//----------------------------------------------------------------------------------------
// \!fn Real EquationOfState::SoundSpeed(Real prim[NHYDRO])
// \brief returns adiabatic sound speed given vector of primitive variables
KOKKOS_INLINE_FUNCTION
Real SoundSpeed(const Real prim[NHYDRO]) const {
  return std::sqrt(gamma_*prim[IPR]/prim[IDN]);
}

//----------------------------------------------------------------------------------------
// \!fn Real EquationOfState::FastMagnetosonicSpeed(const Real prim[], const Real bx)
// \brief returns fast magnetosonic speed given vector of primitive variables
// Note the formula for (C_f)^2 is positive definite, so this func never returns a NaN
KOKKOS_INLINE_FUNCTION
Real FastMagnetosonicSpeed(const Real prim[(NWAVE)], const Real bx) const {
  Real asq = gamma_*prim[IPR];
  Real vaxsq = bx*bx;
  Real ct2 = (prim[IBY]*prim[IBY] + prim[IBZ]*prim[IBZ]);
  Real qsq = vaxsq + ct2 + asq;
  Real tmp = vaxsq + ct2 - asq;
  return std::sqrt(0.5*(qsq + std::sqrt(tmp*tmp + 4.0*asq*ct2))/prim[IDN]);
}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsSR(Real, Real, Real, Real, Real *, Real *) const {return;}

void FastMagnetosonicSpeedsSR(const AthenaArray<Real> &,
  const AthenaArray<Real> &, int, int, int, int, int, AthenaArray<Real> &,
  AthenaArray<Real> &) const {return;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real *, Real *) const
  {return;}

KOKKOS_INLINE_FUNCTION
void FastMagnetosonicSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real,
  Real *, Real *) const {return;}

#endif // EOS_ADIABATIC_MHD_HPP_

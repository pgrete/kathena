//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file few_modes_turbulence.cpp
//  \brief implementation of functions in class Turbulence

// C/C++ headers
#include <iostream>
#include <sstream>    // sstream
#include <stdexcept>  // runtime_error
#include <string>     // c_str()
#include <cmath>
#include <algorithm>

// Athena++ headers
#include "few_modes_turbulence.hpp"
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../mesh/mesh.hpp"
#include "../coordinates/coordinates.hpp"
#include "../globals.hpp"
#include "../hydro/hydro.hpp"
#include "../utils/utils.hpp"

//----------------------------------------------------------------------------------------
//! \fn FewModesTurbulenceDriver::FewModesTurbulenceDriver(Mesh *pm,
//                                                         ParameterInput *pin,
//                                                         int64_t rseed_in)
//  \brief FewModesTurbulenceDriver constructor. If rseed_in != -1 -> restarted sim.

FewModesTurbulenceDriver::FewModesTurbulenceDriver(Mesh *pm,
                                                   ParameterInput *pin,
                                                   int64_t rseed_in) {
  rseed = pin->GetOrAddInteger("problem","rseed",-1); // seed for random number.

  kpeak = pin->GetOrAddReal("problem","kpeak",0.0); // peak of the forcing spec
  accel_rms = pin->GetReal("problem","accel_rms"); // turbulence amplitude
  tcorr = pin->GetReal("problem","corr_time"); // forcing autocorrelation time
  sol_weight = pin->GetReal("problem","sol_weight"); // solenoidal weight
  num_modes = pin->GetInteger("problem","num_modes"); // number of wavemodes

  if ((num_modes > 100) && (Globals::my_rank == 0)) {
    std::cout << "### WARNING using more than 100 explicit modes will significantly "
              << "increase the runtime." << std::endl
              << "If many modes are required in the acceleration field consider using "
              << "the driving mechanism based on full FFTs." << std::endl;
  }

  if (pm->fmturb_flag == 0) {
    std::stringstream msg;
    msg << "### FATAL ERROR in FewModesTurbulenceDriver::TurbulenceDriver" << std::endl
        << "Turbulence flag is set to zero! Shouldn't reach here!" << std::endl;
    throw std::runtime_error(msg.str().c_str());
  }

  if (pm->GetNumMeshBlocksThisRank(Globals::my_rank) != 1) {
    std::stringstream msg;
    msg << "### FATAL ERROR in FewModesTurbulenceDriver::TurbulenceDriver" << std::endl
        << "Currently only one meshblock per process is supported!" << std::endl;
    throw std::runtime_error(msg.str().c_str());
  }

  auto Lx = pin->GetReal("mesh","x1max") - pin->GetReal("mesh","x1min");
  auto Ly = pin->GetReal("mesh","x2max") - pin->GetReal("mesh","x2min");
  auto Lz = pin->GetReal("mesh","x3max") - pin->GetReal("mesh","x3min");
  if ((Lx != 1.0) || (Ly != 1.0) || (Lz != 1.0)) {
    std::stringstream msg;
    msg << "### FATAL ERROR in FewModesTurbulenceDriver::TurbulenceDriver" << std::endl
        << "Only domain sizes with edge lengths of 1 are supported." << std::endl;
    throw std::runtime_error(msg.str().c_str());
  }

  auto gnx1 = pm->mesh_size.nx1;
  auto gnx2 = pm->mesh_size.nx2;
  auto gnx3 = pm->mesh_size.nx3;
  if ((gnx1 != gnx2) || (gnx2 != gnx3)) {
    std::stringstream msg;
    msg << "### FATAL ERROR in FewModesTurbulenceDriver::TurbulenceDriver" << std::endl
        << "Only cubic mesh sizes are supported." << std::endl;
    throw std::runtime_error(msg.str().c_str());
  }

  int nx1=pm->pblock->block_size.nx1;
  int nx2=pm->pblock->block_size.nx2;
  int nx3=pm->pblock->block_size.nx3;

  gis = pm->pblock->loc.lx1 * pm->pblock->block_size.nx1;
  gjs = pm->pblock->loc.lx2 * pm->pblock->block_size.nx2;
  gks = pm->pblock->loc.lx3 * pm->pblock->block_size.nx3;

  this->pm = pm;

  // velocity array does not contain ghost zones
  accel.NewAthenaArray(3,nx3,nx2,nx1);

  // Acceleration field in Fourier space using complex to real transform.
  // using an explicit Kokkos view rather than an AthenaArray as using complex numbers
  // with the templated class led to problems with access Views (return type is double
  // instead of complex)
  accel_hat = Kokkos::View<Complex** , Kokkos::LayoutRight, DevSpace>
    ("accel_hat",3,num_modes);
  accel_hat_new = Kokkos::View<Complex** , Kokkos::LayoutRight, DevSpace>
    ("accel_hat_new",3,num_modes);

  phases_i = Kokkos::View<Complex** , Kokkos::LayoutRight, DevSpace>
    ("phases_i",nx1,num_modes);
  phases_j = Kokkos::View<Complex** , Kokkos::LayoutRight, DevSpace>
    ("phases_j",nx2,num_modes);
  phases_k = Kokkos::View<Complex** , Kokkos::LayoutRight, DevSpace>
    ("phases_k",nx3,num_modes);

  // list of wavenumber vectors
  k_vec = Kokkos::View<Real** , Kokkos::LayoutRight, DevSpace>
    ("k_vec",num_modes,3);

  auto k_vec_host = Kokkos::create_mirror_view(k_vec);
  int k_vec_in[3];
  for (int i = 1; i <= num_modes; i++) {
    pin->GetIntegerVector("modes","k_" + std::to_string(i),k_vec_in);
    k_vec_host(i-1,0) = k_vec_in[0];
    k_vec_host(i-1,1) = k_vec_in[1];
    k_vec_host(i-1,2) = k_vec_in[2];
  }

  Kokkos::deep_copy(k_vec,k_vec_host);

  SetPhases();

  // if this is a restarted simulation recover acc field and current random seed
  if (rseed_in != -1) {
    RestoreFromRestart(rseed_in);
  }
}

// destructor
FewModesTurbulenceDriver::~FewModesTurbulenceDriver() {
}

//----------------------------------------------------------------------------------------
//! \fn void FewModesTurbulenceDriver::RestoreFromRestart(int64_t rseed_in)
//  \brief Restores the random seed and spectral acc field from restart dumps

void FewModesTurbulenceDriver::RestoreFromRestart(int64_t rseed_in) {
  // fast forward original seed to current seed
  // (this way we don't need to save the full ran2 state)
  while (rseed_in != rseed) {
    ran2(&rseed);
  }

  auto num_modes = this->num_modes;
  auto accel_hat = this->accel_hat;
  auto ruser_meshblock_data = pm->pblock->ruser_meshblock_data[0].get_KView3D();
  Kokkos::parallel_for("Copy ruser_meshblock_data to accel_hat",
  Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0}, {3,num_modes}, {1,num_modes}),
  KOKKOS_LAMBDA (int n, int m) {
    accel_hat(n,m) = Complex(
      ruser_meshblock_data(n,m,0),
      ruser_meshblock_data(n,m,1));
  });
}

//----------------------------------------------------------------------------------------
//! \fn void FewModesTurbulenceDriver::SetPhases(void)
//  \brief Precalculate phase factors used in the inverse transform

void FewModesTurbulenceDriver::SetPhases(void) {
  Complex I(0.0,1.0);
  auto phases_i = this->phases_i;
  auto phases_j = this->phases_j;
  auto phases_k = this->phases_k;
  auto gks = this->gks;
  auto gjs = this->gjs;
  auto gis = this->gis;
  auto gnx1 = this->pm->mesh_size.nx1;
  auto gnx2 = this->pm->mesh_size.nx2;
  auto gnx3 = this->pm->mesh_size.nx3;
  int nx1=this->pm->pblock->block_size.nx1;
  int nx2=this->pm->pblock->block_size.nx2;
  int nx3=this->pm->pblock->block_size.nx3;

  auto k_vec = this->k_vec;
  auto num_modes = this->num_modes;

  Kokkos::parallel_for("forcing: calc phases_i",nx1,
  KOKKOS_LAMBDA (int i) {
    Real gi = static_cast<Real>(i+gis);
    Real w_kx;

    for (int m = 0; m < num_modes; m++) {
      w_kx = k_vec(m,0) * 2. * PI / static_cast<Real>(gnx1);
      // adjust phase factor to Complex->Real IFT: u_hat*(k) = u_hat(-k)
      if (k_vec(m,0) == 0.0) {
        phases_i(i,m) = 0.5 * Kokkos::exp(I*w_kx*gi);
      } else {
        phases_i(i,m) = Kokkos::exp(I*w_kx*gi);
      }
    }
  });

  Kokkos::parallel_for("forcing: calc phases_j",nx2,
  KOKKOS_LAMBDA (int j) {
    Real gj = static_cast<Real>(j+gjs);
    Real w_ky;

    for (int m = 0; m < num_modes; m++) {
      w_ky = k_vec(m,1) * 2. * PI / static_cast<Real>(gnx2);
      phases_j(j,m) = Kokkos::exp(I*w_ky*gj);
    }
  });

  Kokkos::parallel_for("forcing: calc phases_k",nx3,
  KOKKOS_LAMBDA (int k) {
    Real gk = static_cast<Real>(k+gks);
    Real w_kz;

    for (int m = 0; m < num_modes; m++) {
      w_kz = k_vec(m,2) * 2. * PI / static_cast<Real>(gnx3);
      phases_k(k,m) = Kokkos::exp(I*w_kz*gk);
    }
  });

}

//----------------------------------------------------------------------------------------
//! \fn void FewModesTurbulenceDriver::Driving(void)
//  \brief Generate and Perturb the velocity field

void FewModesTurbulenceDriver::Driving(void) {

  // evolve forcing
  Generate(pm->dt);

  switch(pm->fmturb_flag) {
    case 1: // fmturb_flag == 1 : continuously driven turbulence
      Perturb(pm->dt);
      break;
    default:
      std::stringstream msg;
      msg << "### FATAL ERROR in FewModesTurbulenceDriver::Driving" << std::endl
          << "Turbulence flag " << pm->fmturb_flag << " is not supported!" << std::endl;
      throw std::runtime_error(msg.str().c_str());
  }

  return;

}

//----------------------------------------------------------------------------------------
//! \fn void FewModesTurbulenceDriver::Generate()
//  \brief Generate velocity pertubation.

void FewModesTurbulenceDriver::Generate(Real dt) {

  // redefine local vars so that they are properly captured by the lambda
  int nx1=pm->pblock->block_size.nx1;
  int nx2=pm->pblock->block_size.nx2;
  int nx3=pm->pblock->block_size.nx3;

  auto accel = this->accel.get_KView4D();
  auto accel_hat = this->accel_hat;
  auto accel_hat_new = this->accel_hat_new;
  auto phases_i = this->phases_i;
  auto phases_j = this->phases_j;
  auto phases_k = this->phases_k;
  auto num_modes = this->num_modes;
  auto k_vec = this->k_vec;
  Complex I(0.0,1.0);
  // get a set of random numbers from the CPU so that they are deterministic
  // when run on GPUs
  Kokkos::View<Real***,
    Kokkos::LayoutRight, DevSpace> random_num("random_num",3,num_modes,2);

  auto random_num_host = Kokkos::create_mirror_view(random_num);

  Real v1, v2, v_sqr;
  for (int n = 0; n < 3; n++)
    for (int m = 0; m < num_modes; m++) {
      do {
        v1 = 2.0* ran2(&rseed) - 1.0;
        v2 = 2.0* ran2(&rseed) - 1.0;
        v_sqr = v1*v1+v2*v2;
      } while (v_sqr >= 1.0 || v_sqr == 0.0);

      random_num_host(n,m,0) = v1;
      random_num_host(n,m,1) = v2;

        }
  Kokkos::deep_copy(random_num,random_num_host);

  auto kpeak = this->kpeak;
  // generate new power spectrum (injection)
  Kokkos::parallel_for("forcing: new power spec",
  Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{3,num_modes},{1,num_modes}),
  KOKKOS_LAMBDA (int n, int m) {
    Real kmag, tmp, norm, v_sqr;
    
    Real kx = k_vec(m,0);
    Real ky = k_vec(m,1);
    Real kz = k_vec(m,2);

    kmag = std::sqrt(kx*kx + ky*ky + kz*kz);

    accel_hat_new(n,m) = Complex(0.,0.);

    tmp = std::pow(kmag/kpeak,2.)*(2.-std::pow(kmag/kpeak,2.));
    if (tmp < 0.)
      tmp = 0.;
    v_sqr = SQR(random_num(n,m,0)) + SQR(random_num(n,m,1));
    norm = std::sqrt(-2.0*std::log(v_sqr)/v_sqr);

    accel_hat_new(n,m) = Complex(
      tmp * norm * random_num(n,m,0),
      tmp * norm * random_num(n,m,1));
  });

  // enforce symmetry of complex to real transform
  Kokkos::parallel_for("forcing: enforce symmetry",
  Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{3,num_modes},{1,num_modes}),
  KOKKOS_LAMBDA(int n, int m) {
    if (k_vec(m,0) == 0.) {
      for (int m2 = 0; m2 < m; m2++) {
        if (k_vec(m,1) == -k_vec(m2,1) && k_vec(m,2) == -k_vec(m2,2))
          accel_hat_new(n,m) =
            Complex(accel_hat_new(n,m2).real(),-accel_hat_new(n,m2).imag());

      }
    }
  });

  auto sol_weight = this->sol_weight;
  // project
  Kokkos::parallel_for("forcing: projection",num_modes,
  KOKKOS_LAMBDA (int m) {
    Real kmag;

    Real kx = k_vec(m,0);
    Real ky = k_vec(m,1);
    Real kz = k_vec(m,2);

    kmag = std::sqrt(kx*kx + ky*ky + kz*kz);

    // setting kmag to 1 as a "continue" doesn't work within the parallel_for construct
    // and it doesn't affect anything (there should never be power in the k=0 mode)
    if (kmag == 0.)
      kmag = 1.;

    // make it a unit vector
    kx /= kmag;
    ky /= kmag;
    kz /= kmag;

    Complex dot(
      accel_hat_new(0,m).real()*kx +
      accel_hat_new(1,m).real()*ky +
      accel_hat_new(2,m).real()*kz,
      accel_hat_new(0,m).imag()*kx +
      accel_hat_new(1,m).imag()*ky +
      accel_hat_new(2,m).imag()*kz);

    accel_hat_new(0,m) = Complex(
      accel_hat_new(0,m).real()*sol_weight + (1. - 2.*sol_weight)*dot.real()*kx,
      accel_hat_new(0,m).imag()*sol_weight + (1. - 2.*sol_weight)*dot.imag()*kx);
    accel_hat_new(1,m) = Complex(
      accel_hat_new(1,m).real()*sol_weight + (1. - 2.*sol_weight)*dot.real()*ky,
      accel_hat_new(1,m).imag()*sol_weight + (1. - 2.*sol_weight)*dot.imag()*ky);
    accel_hat_new(2,m) = Complex(
      accel_hat_new(2,m).real()*sol_weight + (1. - 2.*sol_weight)*dot.real()*kz,
      accel_hat_new(2,m).imag()*sol_weight + (1. - 2.*sol_weight)*dot.imag()*kz);
  });


  // evolve
  Real c_drift = std::exp(-dt/tcorr);
  Real c_diff  = std::sqrt(1.0 - c_drift*c_drift);
  
  Kokkos::parallel_for("forcing: evolve spec",
  Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0},{3,num_modes},{1,num_modes}),
  KOKKOS_LAMBDA (int n, int m) {
    accel_hat(n,m) = Complex(
      accel_hat(n,m).real()*c_drift + accel_hat_new(n,m).real()*c_diff,
      accel_hat(n,m).imag()*c_drift + accel_hat_new(n,m).imag()*c_diff);
  });

  // implictly assuming cubic box of size L=1
  athena_for("Inverse FT",0,2,0,nx3-1,0,nx2-1,0,nx1-1,
  KOKKOS_LAMBDA (int n, int k, int j, int i) {
    Complex phase;
    accel(n,k,j,i) = 0.0;

    for (int m = 0; m < num_modes; m++) {
      phase = phases_i(i,m) * phases_j(j,m) * phases_k(k,m);
      accel(n,k,j,i) += 2.*(
        accel_hat(n,m).real() * phase.real() -
        accel_hat(n,m).imag() * phase.imag());
    }
  });

}

//----------------------------------------------------------------------------------------
//! \fn void FewModesTurbulenceDriver::Perturb(Real dt)
//  \brief Add velocity perturbation to the hydro variables

void FewModesTurbulenceDriver::Perturb(Real dt) {
  std::stringstream msg;
  
  int is=pm->pblock->is, ie=pm->pblock->ie;
  int js=pm->pblock->js, je=pm->pblock->je;
  int ks=pm->pblock->ks, ke=pm->pblock->ke;

  Kokkos::View<Real[4],Kokkos::LayoutRight, DevSpace>
    m("mean momentum (local)"), gm("mean momentum (global)");

  auto u = pm->pblock->phydro->u.get_KView4D();
  auto accel = this->accel.get_KView4D();

  // These reductions are really ugly -- waiting for the Kokkos team for a 
  // streamlined version of reductions with scalar array, see
  // https://github.com/kokkos/kokkos/issues/2013 and
  // https://github.com/kokkos/kokkos/issues/2045
  
  Complex sum_0(0.,0.), sum_1(0.,0.);
  Kokkos::Sum<Complex> sum_reducer_0(sum_0), sum_reducer_1(sum_1);
  
  Kokkos::parallel_reduce("forcing: calc mean momentum 0/1",
    Kokkos::MDRangePolicy< Kokkos::Rank<3> >({ks,js,is},{ke+1,je+1,ie+1},{1,1,ie+1-is}),
    KOKKOS_LAMBDA(int k, int j, int i, Complex& sum_0) {
    Real den = u(IDN,k,j,i);
    sum_0 += Complex(den,den*accel(0,k-ks,j-js,i-is));
  },sum_reducer_0);
  Kokkos::parallel_reduce("forcing: calc mean momentum 1/1",
    Kokkos::MDRangePolicy< Kokkos::Rank<3> >({ks,js,is},{ke+1,je+1,ie+1},{1,1,ie+1-is}),
    KOKKOS_LAMBDA(int k, int j, int i, Complex& sum_1) {
    Real den = u(IDN,k,j,i);
    sum_1 += Complex(
      den*accel(1,k-ks,j-js,i-is),den*accel(2,k-ks,j-js,i-is));
  },sum_reducer_1);

  Kokkos::parallel_for("forcing: copy sum reducers to view",1,
  KOKKOS_LAMBDA(int i) {
    m(0) = sum_0.real();
    m(1) = sum_0.imag();
    m(2) = sum_1.real();
    m(3) = sum_1.imag();
  });

#ifdef MPI_PARALLEL
  int mpierr;

// Sum the perturbations over all processors
  mpierr = MPI_Allreduce(m.data(), gm.data(), 4, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (mpierr) {
    msg << "[normalize]: MPI_Allreduce error = "
        << mpierr << std::endl;
    throw std::runtime_error(msg.str().c_str());
  }
#endif // MPI_PARALLEL

  Real ampl_sum = 0.;
  Kokkos::Sum<Real> ampl_sum_reducer(ampl_sum);
  Kokkos::parallel_reduce("forcing: remove mean momentum and calc norm",
    Kokkos::MDRangePolicy< Kokkos::Rank<4> >({0,0,0,0},
                                             {3,ke-ks+1,je-js+1,ie-is+1},
                                             {1,1,1,ie+1-is}),
    KOKKOS_LAMBDA(int n, int k, int j, int i, Real& ampl_sum) {
#ifdef MPI_PARALLEL
      accel(n,k,j,i) -= gm(n+1)/gm(0);
#else
      accel(n,k,j,i) -= m(n+1)/m(0);
#endif // MPI_PARALLEL
      ampl_sum += SQR(accel(n,k,j,i));
  },ampl_sum_reducer);

  Kokkos::parallel_for("forcing: copy ampl reducer to view",1,
  KOKKOS_LAMBDA(int i) {
    m(0) = ampl_sum;
  });



#ifdef MPI_PARALLEL
  // Sum the perturbations over all processors
  mpierr = MPI_Allreduce(m.data(), gm.data(), 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (mpierr) {
    msg << "[normalize]: MPI_Allreduce error = "
        << mpierr << std::endl;
    throw std::runtime_error(msg.str().c_str());
  }
#endif // MPI_PARALLEL

  auto accel_rms = this->accel_rms;
  Real gsize = (static_cast<Real>(pm->mesh_size.nx1) * 
                static_cast<Real>(pm->mesh_size.nx2) *
                static_cast<Real>(pm->mesh_size.nx3));

  athena_for("apply momemtum perturb",ks,ke,js,je,is,ie,
  KOKKOS_LAMBDA(int k, int j, int i) {
#ifdef MPI_PARALLEL
    Real norm = accel_rms/std::sqrt(gm(0)/gsize);
#else
    Real norm = accel_rms/std::sqrt(m(0)/gsize);
#endif // MPI_PARALLEL

    // normalizing accel field here so that the actual values are used in the output
    accel(0,k-ks,j-js,i-is) *= norm;
    accel(1,k-ks,j-js,i-is) *= norm;
    accel(2,k-ks,j-js,i-is) *= norm;

    Real qa = dt * u(IDN,k,j,i);
    if (NON_BAROTROPIC_EOS) {
      u(IEN,k,j,i) += (
        u(IM1,k,j,i) * dt * accel(0,k-ks,j-js,i-is) +
        u(IM2,k,j,i) * dt * accel(1,k-ks,j-js,i-is) +
        u(IM3,k,j,i) * dt * accel(2,k-ks,j-js,i-is) + (
          SQR(accel(0,k-ks,j-js,i-is)) + 
          SQR(accel(1,k-ks,j-js,i-is)) +
          SQR(accel(2,k-ks,j-js,i-is))) * qa * qa / (2*u(IDN,k,j,i)));
    }

    u(IM1,k,j,i) += qa * accel(0,k-ks,j-js,i-is);
    u(IM2,k,j,i) += qa * accel(1,k-ks,j-js,i-is);
    u(IM3,k,j,i) += qa * accel(2,k-ks,j-js,i-is);
  });

  return;

}
//----------------------------------------------------------------------------------------
//! \fn void FewModesTurbulenceDriver::CopyAccelToOutputVars(
//        AthenaArray<Real> &user_out_var)
//  \brief Write acceleration field to output buffer

void FewModesTurbulenceDriver::CopyAccelToOutputVars(AthenaArray<Real> &user_out_var) {

  auto user_out_var_view = user_out_var.get_KView4D();
  auto accel = this->accel.get_KView4D();

  //ks,ke ... are member variables, and so need to be aliased to be
  //captured by the lambda
  auto is=pm->pblock->is, ie=pm->pblock->ie;
  auto js=pm->pblock->js, je=pm->pblock->je;
  auto ks=pm->pblock->ks, ke=pm->pblock->ke;

  athena_for("Copy acceleration field for analysis dump",0,2,ks,ke,js,je,is,ie,
  KOKKOS_LAMBDA (int n, int k, int j, int i) {
    user_out_var_view(n,k,j,i) = accel(n,k-ks,j-js,i-is);
  });

  auto num_modes = this->num_modes;
  auto accel_hat = this->accel_hat;
  auto ruser_meshblock_data = pm->pblock->ruser_meshblock_data[0].get_KView3D();

  Kokkos::parallel_for("Copy accel_hat to ruser_meshblock_data",
  Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0,0}, {3,num_modes}, {1,num_modes}),
  KOKKOS_LAMBDA (int n, int m) {
    ruser_meshblock_data(n,m,0) = accel_hat(n,m).real();
    ruser_meshblock_data(n,m,1) = accel_hat(n,m).imag();
  });

  pm->rseed_rst = this->rseed;

  return;
}

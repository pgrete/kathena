//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file buffer_utils.cpp
//  \brief namespace containing buffer utilities.

#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "buffer_utils.hpp"

namespace BufferUtility {

//----------------------------------------------------------------------------------------
//! \fn void Pack4DData(AthenaArray<Real> &src, Real *buf, int sn, int en,
//                     int si, int ei, int sj, int ej, int sk, int ek, int &offset)
//  \brief pack a 4D AthenaArray into a one-dimensional buffer

void Pack4DData(AthenaArray<Real> &src, Real *buf, int sn, int en,
                int si, int ei, int sj, int ej, int sk, int ek, int &offset) {
  for (int n=sn; n<=en; ++n) {
    for (int k=sk; k<=ek; k++) {
      for (int j=sj; j<=ej; j++) {
#pragma omp simd
        for (int i=si; i<=ei; i++)
            buf[offset++]=src(n,k,j,i);
      }
    }
  }
}

//----------------------------------------------------------------------------------------
//! \fn void Unpack4DData(Real *buf, AthenaArray<Real> &dst, int sn, int en,
//                        int si, int ei, int sj, int ej, int sk, int ek, int &offset)
//  \brief unpack a one-dimensional buffer into a 4D AthenaArray

void Unpack4DData(Real *buf, AthenaArray<Real> &dst, int sn, int en,
                  int si, int ei, int sj, int ej, int sk, int ek, int &offset) {
  for (int n=sn; n<=en; ++n) {
    for (int k=sk; k<=ek; ++k) {
      for (int j=sj; j<=ej; ++j) {
#pragma omp simd
        for (int i=si; i<=ei; ++i)
          dst(n,k,j,i) = buf[offset++];
      }
    }
  }
  return;
}

//----------------------------------------------------------------------------------------
//! \fn void Pack3DData(AthenaArray<Real> &src, Real *buf,
//                      int si, int ei, int sj, int ej, int sk, int ek, int &offset)
//  \brief pack a 3D AthenaArray into a one-dimensional buffer

void Pack3DData(AthenaArray<Real> &src, Real *buf,
                int si, int ei, int sj, int ej, int sk, int ek, int &offset) {
  for (int k=sk; k<=ek; k++) {
    for (int j=sj; j<=ej; j++) {
#pragma omp simd
      for (int i=si; i<=ei; i++)
          buf[offset++]=src(k, j, i);
    }
  }
  return;
}

//----------------------------------------------------------------------------------------
//! \fn void Unpack3DData(Real *buf, AthenaArray<Real> &dst,
//                        int si, int ei, int sj, int ej, int sk, int ek, int &offset)
//  \brief unpack a one-dimensional buffer into a 3D AthenaArray

void Unpack3DData(Real *buf, AthenaArray<Real> &dst,
                  int si, int ei, int sj, int ej, int sk, int ek, int &offset) {
  for (int k=sk; k<=ek; ++k) {
    for (int j=sj; j<=ej; ++j) {
#pragma omp simd
      for (int i=si; i<=ei; ++i)
        dst(k,j,i) = buf[offset++];
    }
  }
  return;
}
//----------------------------------------------------------------------------------------
//! \fn void Pack4DData(AthenaArray<Real> &src, Real *buf, int sn, int en,
//                     int si, int ei, int sj, int ej, int sk, int ek, int &offset)
//  \brief pack a 4D AthenaArray into a one-dimensional buffer

void Pack4DData(AthenaArray<Real> &src_in, Kokkos::View<Real*, DevSpace> &buf, int sn,
                int en, int si, int ei, int sj, int ej, int sk, int ek, int &offset) {

  auto src = src_in.get_KView4D();

  int ni = ei+1-si;
  int nj = ej+1-sj;
  int nk = ek+1-sk;
  int nn = en+1-sn;

  athena_for("Pack4DData",sn,en,sk,ek,sj,ej,si,ei,
  KOKKOS_LAMBDA (int n, int k, int j, int i) {
        buf(offset + i-si + ni*( j-sj + nj*(k-sk + nk*(n-sn))))=src(n,k,j,i);
  });

  offset += nn*nk*nj*ni;
  // need fence here so that te offset is correct for the next call
  Kokkos::fence();

  return;
}

//----------------------------------------------------------------------------------------
//! \fn void Unpack4DData(Real *buf, AthenaArray<Real> &dst, int sn, int en,
//                        int si, int ei, int sj, int ej, int sk, int ek, int &offset)
//  \brief unpack a one-dimensional buffer into a 4D AthenaArray

void Unpack4DData(Kokkos::View<Real*, DevSpace> &buf, AthenaArray<Real> &dst_in, int sn,
                  int en, int si, int ei, int sj, int ej, int sk, int ek, int &offset) {
  auto dst = dst_in.get_KView4D();

  int ni = ei+1-si;
  int nj = ej+1-sj;
  int nk = ek+1-sk;
  int nn = en+1-sn;

  athena_for("Unpack4DData",sn,en,sk,ek,sj,ej,si,ei,
  KOKKOS_LAMBDA (int n, int k, int j, int i) {
       dst(n,k,j,i) =buf(offset + i-si + ni*( j-sj + nj*(k-sk + nk*(n-sn))));
  });

  offset += nn*nk*nj*ni;
  // need fence here so that te offset is correct for the next call
  Kokkos::fence();

  return;
}

//----------------------------------------------------------------------------------------
//! \fn void Pack3DData(AthenaArray<Real> &src, Real *buf,
//                      int si, int ei, int sj, int ej, int sk, int ek, int &offset)
//  \brief pack a 3D AthenaArray into a one-dimensional buffer

void Pack3DData(AthenaArray<Real> &src_in,  Kokkos::View<Real*, DevSpace> &buf,
                int si, int ei, int sj, int ej, int sk, int ek, int &offset) {
  auto src = src_in.get_KView3D();

  int ni = ei+1-si;
  int nj = ej+1-sj;
  int nk = ek+1-sk;

  athena_for("Pack3DData",sk,ek,sj,ej,si,ei,
  KOKKOS_LAMBDA (int k, int j, int i) {
        buf(offset + i-si + ni*( j-sj + nj*(k-sk) ))=src(k,j,i);
  });

  offset += nk*nj*ni;
  // need fence here so that te offset is correct for the next call
  Kokkos::fence();

  return;
}

//----------------------------------------------------------------------------------------
//! \fn void Unpack3DData(Real *buf, AthenaArray<Real> &dst,
//                        int si, int ei, int sj, int ej, int sk, int ek, int &offset)
//  \brief unpack a one-dimensional buffer into a 3D AthenaArray

void Unpack3DData( Kokkos::View<Real*, DevSpace> &buf, AthenaArray<Real> &dst_in,
                  int si, int ei, int sj, int ej, int sk, int ek, int &offset) {
  auto dst = dst_in.get_KView3D();

  int ni = ei+1-si;
  int nj = ej+1-sj;
  int nk = ek+1-sk;

  athena_for("Unpack3DData",sk,ek,sj,ej,si,ei,
  KOKKOS_LAMBDA (int k, int j, int i) {
       dst(k,j,i) = buf(offset + i-si + ni*( j-sj + nj*(k-sk) ));
  });

  offset += nk*nj*ni;
  // need fence here so that te offset is correct for the next call
  Kokkos::fence();

  return;
}

} // end namespace BufferUtility

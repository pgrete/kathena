# Regression test for driven turbulence problem
#
# Runs driven turbulence test using MPI and restarting
# Compares final statistics from user hst output to reference data.

# Modules
import numpy as np
import sys
import scripts.utils.athena as athena
sys.path.insert(0, '../../vis/python')
import athena_read # noqa


# Prepare Athena++
def prepare(**kwargs):
    athena.configure('b', 'mpi',
                     prob='fmturb',
                     flux='roe', **kwargs)
    athena.make()


# Run Athena++
def run(**kwargs):
    arguments = ['time/ncycle_out=0', 'time/tlim=1.02',
                 'output2/dt=-1', 'output3/dt=-1',
                 'meshblock/nx2=32', 'meshblock/nx3=32',
                 ]
    athena.mpirun(kwargs['mpirun_cmd'], kwargs['mpirun_opts'], 4,
                  'mhd/athinput.fmturb', arguments)
    arguments = ['time/tlim=3.0']
    athena.mpirestart(kwargs['mpirun_cmd'], kwargs['mpirun_opts'], 4,
                      'Turb.00001.rst', arguments)


# Analyze outputs
def analyze():
    # check hst output. Just checking last line
    with open('bin/Turb.hst', 'r') as fh:
        for line in fh:
            pass

    line_data = np.fromstring(line, dtype=float, sep=' ')
    try:
        assert(line_data[-1] == 2.63341e+03)
        assert(line_data[-2] == 4.20846e-03)
        assert(line_data[-3] == 1.27494e-01)
        assert(line_data[-4] == 1.15473e+01)
        assert(line_data[-5] == 4.76524e-01)
    except AssertionError:
        print('!!! ERROR: HST output mismatch')
        print(line_data)
        return False
    return True

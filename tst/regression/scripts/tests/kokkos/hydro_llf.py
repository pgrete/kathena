# Test script for Kokkosized hydro LLF Riemann solver

# Modules
import numpy as np
import sys
import scripts.utils.athena as athena
sys.path.insert(0, '../../vis/python')
import athena_read # noqa


# Prepare Athena++
def prepare(**kwargs):
    athena.configure(prob='blast',
                     coord='cartesian',
                     flux='llf', **kwargs)
    athena.make()


# Run Athena++
def run(**kwargs):
    arguments = [
      'job/problem_id=hydro.llf.adiabatic.Blast',
      'output2/dt=0.1',
      'time/tlim=0.4']
    athena.run('hydro/athinput.blast', arguments)


# Analyze outputs
def analyze():
    headers = ['rho', 'press', 'vel']
    _, _, _, data_ref = athena_read.vtk(
        'data/hydro.llf.adiabatic.Blast.block0.out2.00004.vtk')
    _, _, _, data_new = athena_read.vtk(
        'bin/hydro.llf.adiabatic.Blast.block0.out2.00004.vtk')

    for header in headers:
        try:
            # vtk data is single precision only, thus decimal 7 should suffice
            np.testing.assert_almost_equal(data_new[header], data_ref[header], decimal=7)
        except AssertionError:
            print('!!! ERROR: ' + header + ' arrays are not identical.')
            return False

    return True
